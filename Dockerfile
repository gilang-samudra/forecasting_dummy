# syntax=docker/dockerfile:1
FROM python:3.9-slim-buster AS yzf-daily
WORKDIR /app
RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    libgomp1
RUN apt-get -y install gcc
RUN python -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
# Create the environment:
COPY . .
CMD ["python3", "src/main.py"]

# 28d dockerfile
FROM python:3.9-slim-buster AS yzf-daily-28d
WORKDIR /app
RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    libgomp1
RUN apt-get -y install gcc
RUN python -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
# Create the 28d environment:
COPY . .
CMD ["python3", "src/main_28d.py"]