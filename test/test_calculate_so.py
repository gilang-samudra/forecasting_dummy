import numpy as np
import pandas as pd
import pytest
from src.so_calculation.calculate_so import SOCalculationV1, SOCalculationV4
from src.utils import generate_date


class TestSOCalculation:
    @pytest.fixture(autouse=True)
    def _add_dummy(self) -> None:
        # mapping columns data type
        col_type = {
            "id": "category",
            "product_plu": "object",
            "delivery_date": "datetime64[ns]",
            "prev_delivery_date": "datetime64[ns]",
            "prev_2_delivery_date": "datetime64[ns]",
            "next_delivery_date": "datetime64[ns]",
        }

        # load sample forecast and sales order data
        self.sales = pd.read_csv("test/data/general/sales_training_data.zip")
        self.so = pd.read_csv("test/data/general/sales_order_training_data.zip")

        self.forecast_v1 = pd.read_csv(
            "test/data/so_calculation/calculate_so/forecast_sample_v1.csv"
        )
        self.forecast_v1 = self.forecast_v1.astype(col_type)
        self.forecast_v4 = pd.read_csv(
            "test/data/so_calculation/calculate_so/forecast_sample_v4.csv"
        )
        self.forecast_v4 = self.forecast_v4.astype(col_type)

        # generate required data
        self.new_products = {
            "2022-11-04": ["660039", "660042"],
            "2022-11-05": ["660042"],
        }
        today_date = pd.to_datetime("2022-11-01")
        start_date = "2021-11-01"
        self.windows = generate_date(current_date=today_date, train_start=start_date)

        self.so_calculation_v1 = SOCalculationV1(
            forecast=self.forecast_v1,
            sales_order=self.so,
            new_products=self.new_products,
            windows=self.windows,
        )
        self.so_calculation_v4 = SOCalculationV4(
            forecast=self.forecast_v4,
            sales=self.sales,
            sales_order=self.so,
            new_products=self.new_products,
            windows=self.windows,
        )

        # load expected result of v1 and v4
        self.so_v1 = pd.read_pickle(
            "test/data/so_calculation/calculate_so/expected_so_v1.pkl"
        )
        self.so_v4 = pd.read_pickle(
            "test/data/so_calculation/calculate_so/expected_so_v4.pkl"
        )

    def test_compute(self) -> None:
        # so calculation v1 and v4
        so_v1, trend_input_v1 = self.so_calculation_v1.calculate()
        so_v4, trend_input_v4 = self.so_calculation_v4.calculate()

        # testing qty adjustment and shape
        assert so_v1.shape[1] == so_v4.shape[1] == 22

        # checking for duplicate ID-delivery date rows
        assert so_v1.duplicated(subset=["id", "delivery_date"]).sum() == 0
        assert so_v4.duplicated(subset=["id", "delivery_date"]).sum() == 0
        assert trend_input_v1.duplicated(subset=["id", "start_at"]).sum() == 0
        assert trend_input_v4.duplicated(subset=["id", "start_at"]).sum() == 0

        so_v1 = so_v1.drop("forecast_type", axis=1)
        so_v4 = so_v4.drop("forecast_type", axis=1)

        so_all = [so_v1, so_v4]
        forecast_all = [self.forecast_v1, self.forecast_v4]

        for i in range(2):
            so_temp = so_all[i].copy()
            so_temp["outlet_code"] = so_temp["id"].apply(lambda id: id.split("_")[1])
            so_temp["sum"] = so_temp.groupby("outlet_code")["SO"].sum()

            assert (
                len(so_temp.loc[(so_temp["sum"] > 0) & (so_temp["sum"] < 8)]) == 0
            )  # minimum 8pcs/outlet
            assert so_temp.groupby(["id", "delivery_date"]).ngroups == so_temp.shape[0]

        # testing id and delivery date result
        assert (
            (so_v1[["id", "delivery_date"]] == self.so_v1[["id", "delivery_date"]])
            .all()
            .all()
        )
        assert (
            (so_v4[["id", "delivery_date"]] == self.so_v4[["id", "delivery_date"]])
            .all()
            .all()
        )

        # testing nan values
        all_df = [so_v1, so_v4, trend_input_v1, trend_input_v4]
        for temp_df in all_df:
            temp_df = temp_df.replace({"": np.nan})

            assert temp_df.isna().sum().sum() == 0
