import pandas as pd
import numpy as np
import pytest

import src.training.train_bottom_up as train_bu
from src.utils import generate_date
from src.feature_engineering.feature_bottom_up import label_encoding


class TestTrainingBottomUp:
    @pytest.fixture(autouse=True)
    def _add_dummy(self) -> None:
        # load feature engineered sales data (equals number of d)
        self.sales = pd.read_csv("test/data/general/sales_training_data_fe.csv")
        self.sales_preprocessed = pd.read_csv(
            "test/data/general/sales_training_data_preprocessed.csv"
        )

        # cast object to category dtype
        self.sales_preprocessed[
            self.sales_preprocessed.select_dtypes(["object"]).columns
        ] = self.sales_preprocessed.select_dtypes(["object"]).apply(
            lambda x: x.astype("category")
        )

        # generate conv_id
        _, self.conv_id, _ = label_encoding(self.sales_preprocessed.copy())

    def test_train_and_predict(self) -> None:
        df_dummy = self.sales.copy()

        # Generate training and submission window
        today_date = pd.to_datetime("2022-10-01")
        start_date = "2021-10-01"
        windows = generate_date(current_date=today_date, train_start=start_date)
        _, _, forecast_start, forecast_end = windows

        submission_range = [
            x.strftime("%Y-%m-%d")
            for x in pd.date_range(start=forecast_start, end=forecast_end)
        ]
        n_days = len(submission_range)

        df_result = train_bu.train_and_predict(df_dummy, self.conv_id, n_days)

        # testing the result
        assert df_result.select_dtypes(include=["object", "category"]).shape[1] == 1
        assert df_result.select_dtypes(include=["float"]).shape[1] == n_days
        assert (df_result.select_dtypes(include=["float"]) >= 0).all
        # ensure all ids are forecasted and unique
        assert df_result.shape[0] == self.sales["id"].nunique()

    def test_train(self) -> None:
        df_dummy = self.sales.copy()
        df_dummy_test = self.sales.drop("sales", axis=1).copy()

        sample_outlet = np.random.choice(df_dummy["outlet_code"].values, 1)[0]
        df_dummy_outlet = df_dummy.loc[df_dummy["outlet_code"] == sample_outlet]

        df_result_series = train_bu.train(df_dummy_test, df_dummy_outlet, sample_outlet)

        assert df_result_series.index.equals(
            df_dummy_test.loc[df_dummy_test["outlet_code"] == sample_outlet].index
        )
        assert (df_result_series >= 0).all
