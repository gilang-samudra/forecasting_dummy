import csv
import datetime
import logging

import numpy as np
import pandas as pd
import pytest
from downcast import reduce
from src.preprocessing.preprocess import Preprocess, PromotionGenerator


@pytest.fixture()
def dummy_data():
    train_start = datetime.datetime(2021, 9, 10)
    train_end = datetime.datetime(2021, 9, 15)
    forecast_start = datetime.datetime(2021, 9, 16)
    forecast_end = datetime.datetime(2021, 9, 25)
    sales_init = pd.read_csv("test/data/general/sales_training_data.csv").head(5)
    sales_init = sales_init.drop(["customer_id"], axis=1)
    promo_data = pd.read_csv("test/data/general/promotion_training_data.csv")

    preprocess_helper = Preprocess(
        sales=sales_init,
        promo=promo_data,
        windows=[train_start, train_end, forecast_start, forecast_end],
    )
    logging.info("Execute preprocess helper fixture")
    return preprocess_helper


@pytest.fixture()
def dummy_promotion():
    with open("test/data/preprocess/reversed_date_map_dict.csv") as csv_file:
        reader = csv.reader(csv_file)
        reversed_date_map = dict(reader)

    reversed_date_map = {int(k): v for k, v in reversed_date_map.items()}

    preprocess_sales = pd.read_csv("test/data/general/preprocessed_sales.csv")
    promotion_generator = PromotionGenerator(
        df=preprocess_sales, date_map=reversed_date_map
    )
    logging.info("Execute promotion fixture")
    return promotion_generator


@pytest.fixture()
def dummy_data_complete():
    train_start = datetime.datetime(2021, 9, 11)
    train_end = datetime.datetime(2022, 9, 17)
    forecast_start = datetime.datetime(2022, 9, 18)
    forecast_end = datetime.datetime(2022, 9, 28)
    sales_init = pd.read_csv("test/data/general/sales_training_data.csv")
    sales_init = sales_init.drop(["customer_id"], axis=1)
    promo_data = pd.read_csv("test/data/general/promotion_training_data.csv")

    preprocess = Preprocess(
        sales=sales_init,
        promo=promo_data,
        windows=[train_start, train_end, forecast_start, forecast_end],
    )
    logging.info("Execute preprocess fixture")
    return preprocess


class TestPreprocessHelper:
    def test_create_date_map(self, dummy_data):
        test_date_map = dummy_data.create_date_map()
        expected_date_map = {
            "2021-09-10": 1,
            "2021-09-11": 2,
            "2021-09-12": 3,
            "2021-09-13": 4,
            "2021-09-14": 5,
            "2021-09-15": 6,
            "2021-09-16": 7,
            "2021-09-17": 8,
            "2021-09-18": 9,
            "2021-09-19": 10,
            "2021-09-20": 11,
            "2021-09-21": 12,
            "2021-09-22": 13,
            "2021-09-23": 14,
            "2021-09-24": 15,
            "2021-09-25": 16,
        }
        assert test_date_map == expected_date_map, "wrong date_map result"

    def test_separate_columns(self, dummy_data):

        dummy_data.separate_columns()
        test_id_cols = set(dummy_data.id_cols)
        test_window_cols = dummy_data.window_cols

        expected_id_cols = set(
            [
                "id",
                "dc_code",
                "outlet_code",
                "product_plu",
                "cluster_id",
                "category_id",
                "sub_category_id",
            ]
        )
        expected_window_cols = [
            "2021-09-10",
            "2021-09-11",
            "2021-09-12",
            "2021-09-13",
            "2021-09-14",
            "2021-09-15",
        ]
        assert test_id_cols == expected_id_cols, "wrong id_cols result"
        assert test_window_cols == expected_window_cols, "wrong window_cols result"

    def test_filter_date(self, dummy_data):
        dummy_data.filter_date()
        expected_sales_data = pd.read_csv(
            "test/data/preprocess/expected_sales_data_after_filter_date.csv"
        )
        expected_promo_data = pd.read_csv(
            "test/data/preprocess/expected_promo_after_filter_date.csv"
        )

        expected_promo_data = expected_promo_data.fillna("null")
        dummy_data.promo = dummy_data.promo.fillna("null")

        assert dummy_data.sales.equals(expected_sales_data)
        assert dummy_data.promo.equals(expected_promo_data)

    def test_convert_to_day_number(self, dummy_data):
        input_df = pd.read_csv("test/data/preprocess/input_df_for_date_converter.csv")
        expected_df = pd.read_pickle(
            "test/data/preprocess/expected_df_date_converter.pkl"
        )

        output_df = dummy_data.convert_to_day_number(df=input_df)

        assert output_df.equals(expected_df)


class TestPromotionGenerator:
    def test_get_promo_period(self, dummy_promotion):
        dummy_promotion.get_promo_period()
        during_promo = dummy_promotion.during_promo

        assert during_promo["flag_promo"].sum() == 174

    def test_get_after_promo_period(self, dummy_promotion):
        dummy_promotion.get_after_promo_period()
        after_promo = dummy_promotion.after_promo

        assert after_promo.groupby("id")["flag_after_promo"].sum().min() == 28
        assert after_promo.groupby("id")["flag_after_promo"].sum().max() == 56

    def test_get_after_promo_none(self, dummy_promotion):
        dummy_promotion.df = dummy_promotion.df.loc[dummy_promotion.df.d < 264]
        dummy_promotion.get_after_promo_period()
        after_promo = dummy_promotion.after_promo

        assert after_promo.equals(pd.DataFrame(columns=["id", "d", "flag_after_promo"]))

    def test_generate(self, dummy_promotion):
        during_promo, after_promo = dummy_promotion.generate()

        expected_during_promo = pd.read_csv(
            "test/data/preprocess/expected_during_promo.csv"
        )
        expected_after_promo = pd.read_csv(
            "test/data/preprocess/expected_after_promo.csv"
        )

        expected_during_promo["delivery_date"] = pd.to_datetime(
            expected_during_promo["delivery_date"]
        )
        expected_after_promo["delivery_date"] = pd.to_datetime(
            expected_after_promo["delivery_date"]
        )

        assert during_promo.equals(expected_during_promo)
        assert after_promo.equals(expected_after_promo)


class TestPreprocess:
    def test_generate_time_features(self, dummy_data_complete):
        dummy_data_complete.sales = reduce(dummy_data_complete.sales)
        dummy_data_complete.promo = reduce(dummy_data_complete.promo)

        # filter date
        dummy_data_complete.filter_date()

        # drop NaN id columns and replace NaN sales with 0
        dummy_data_complete.sales.dropna(
            axis=0, subset=dummy_data_complete.id_cols, inplace=True
        )
        dummy_data_complete.sales.replace(np.nan, 0, inplace=True)

        # melt data
        dummy_data_complete.sales = pd.melt(
            dummy_data_complete.sales,
            id_vars=dummy_data_complete.id_cols,
            var_name="date",
            value_name="sales",
        )
        dummy_data_complete.sales["date"] = dummy_data_complete.sales["date"].astype(
            "category"
        )
        dummy_data_complete.generate_time_features()
        time_features = dummy_data_complete.time_features

        expected_time_features = pd.read_csv(
            "test/data/preprocess/expected_time_features.csv"
        )
        expected_time_features = expected_time_features.astype(
            time_features.dtypes.to_dict()
        )
        assert time_features.equals(expected_time_features)

    def test_generate_promo_features(self, dummy_data_complete):
        dummy_data_complete.generate_promo_features()
        promo_features = dummy_data_complete.promo_features

        expected_promo_features = pd.read_csv(
            "test/data/preprocess/expected_promo_features.csv"
        )
        expected_promo_features = expected_promo_features.astype(
            promo_features.dtypes.to_dict()
        )
        expected_promo_features["product_plu"] = expected_promo_features[
            "product_plu"
        ].astype("str")

        assert promo_features.equals(expected_promo_features)

    def test_compute(self, dummy_data_complete):
        dummy_data_complete.compute()
        preprocessed_sales = dummy_data_complete.sales

        expected_preprocessed_sales = pd.read_csv(
            "test/data/general/preprocessed_sales.csv"
        )
        expected_preprocessed_sales["product_plu"] = expected_preprocessed_sales[
            "product_plu"
        ].astype("str")
        expected_preprocessed_sales = expected_preprocessed_sales.astype(
            preprocessed_sales.dtypes.to_dict()
        )

        assert preprocessed_sales.equals(expected_preprocessed_sales)
