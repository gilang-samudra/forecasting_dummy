import pandas as pd
import numpy as np
import pytest

import src.feature_engineering.feature_bottom_up as fe_bu
from src.config import FeatureParam


class TestFeatureEngineering:
    @pytest.fixture(autouse=True)
    def _add_dummy(self) -> None:
        # load 10k rows of preprocessed data
        self.sales = pd.read_csv(
            "test/data/general/sales_training_data_preprocessed.csv"
        )

        # cast object to category dtype
        self.sales[
            self.sales.select_dtypes(["object"]).columns
        ] = self.sales.select_dtypes(["object"]).apply(lambda x: x.astype("category"))

        # arbitrary min_delivery_d value
        self.min_delivery_d = self.sales["d"].max() - 20

    def test_label_encoding(self) -> None:
        df_dummy = self.sales.copy()
        outlets, conv_id, df_result = fe_bu.label_encoding(df_dummy)

        # testing label encoding values
        assert (len(outlets) - df_dummy["outlet_code"].nunique()) == 0
        assert len(set(conv_id.values()) ^ set(self.sales["id"])) == 0
        # testing dataframe shape
        assert df_result.shape == self.sales.shape

    def test_mean_encoding(self) -> None:
        df_dummy = self.sales.copy()
        df_result = fe_bu.mean_encoding(df_dummy)

        list_cols = [
            "id",
            "dc_code",
            "cluster_id",
            "outlet_code",
            "category_id",
            "sub_category_id",
            "product_plu",
        ]

        # testing mean encoding values
        for col in list_cols:
            # sample one type of category
            sample_categ = df_dummy.sample(1)[col].values[0]
            assert df_result.loc[
                df_result[col] == sample_categ, f"{col}_avg_sales"
            ].unique()[0] == np.float32(
                np.mean(df_dummy.loc[df_dummy[col] == sample_categ, "sales"])
            )

        # testing dataframe shape
        assert df_result.shape[0] == self.sales.shape[0]
        assert df_result.shape[1] == self.sales.shape[1] + 7

    def test_sales_feature_engineering(self) -> None:
        df_dummy = self.sales.copy()
        _, __, df_dummy = fe_bu.label_encoding(df_dummy)
        feature_param = FeatureParam()
        df_result = fe_bu.sales_feature_engineering(
            df_dummy, self.min_delivery_d
        ).to_pandas()

        promo_cols = [x for x in df_dummy.columns.tolist() if x.startswith("promo_")]

        # helper function (compare two float if it is close enough)
        def isclose(a, b, rel_tol=1e-06, abs_tol=0.0) -> bool:
            return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

        # testing Lag + EMA values
        for n in feature_param.VALUE:
            # test for all 100 ids
            for sample_id in df_result["id"].unique():
                temp_df_result = df_result.loc[df_result["id"] == sample_id].copy()
                temp_df_origin = df_dummy.loc[(df_dummy["id"] == sample_id)].copy()

                # separate dataframe for non-promo
                temp_df_origin_non_promo = pd.DataFrame()
                for promo in promo_cols:
                    temp_df_origin_non_promo = pd.concat(
                        [
                            temp_df_origin_non_promo,
                            temp_df_origin.loc[temp_df_origin[promo] != 1],
                        ]
                    )

                # generate lag+EMA
                temp_df_origin_non_promo[f"SALES_LAG_{n}"] = (
                    temp_df_origin_non_promo["sales"].shift(n).astype(np.float32)
                )

                temp_df_origin_non_promo[f"SALES_EMA_{n}"] = np.float32(
                    temp_df_origin_non_promo["sales"].shift(n).ewm(com=12).mean()
                )

                max_lag_df_promo = (
                    temp_df_origin_non_promo[f"SALES_LAG_{n}"].copy().max()
                )
                max_EMA_df_promo = (
                    temp_df_origin_non_promo[f"SALES_EMA_{n}"].copy().max()
                )

                # rule d > 20
                temp_df_origin_non_promo = temp_df_origin_non_promo.loc[
                    temp_df_origin_non_promo["d"] > 20
                ].reset_index(drop=True)

                # after promo adjustment
                temp_df_origin_non_promo = temp_df_origin_non_promo.merge(
                    temp_df_result[["id", "d", "flag_after_promo"]],
                    on=["id", "d"],
                    how="left",
                )

                temp_df_origin_non_promo.loc[
                    temp_df_origin_non_promo["flag_after_promo"] == 1, f"SALES_LAG_{n}"
                ] = temp_df_origin_non_promo.loc[
                    temp_df_origin_non_promo["flag_after_promo"] == 1, f"SALES_LAG_{n}"
                ].apply(
                    lambda x: 0.5 * x
                )

                temp_df_origin_non_promo.loc[
                    temp_df_origin_non_promo["flag_after_promo"] == 1, f"SALES_EMA_{n}"
                ] = temp_df_origin_non_promo.loc[
                    temp_df_origin_non_promo["flag_after_promo"] == 1, f"SALES_EMA_{n}"
                ].apply(
                    lambda x: 0.5 * x
                )

                # testing
                assert temp_df_result[f"SALES_LAG_{n}"].sum() == np.float32(
                    temp_df_origin_non_promo[f"SALES_LAG_{n}"].sum()
                    + max_lag_df_promo
                    * (len(temp_df_result) - len(temp_df_origin_non_promo))
                )
                assert isclose(
                    temp_df_result[f"SALES_EMA_{n}"].sum(),
                    np.float32(
                        temp_df_origin_non_promo[f"SALES_EMA_{n}"].sum()
                        + max_EMA_df_promo
                        * (len(temp_df_result) - len(temp_df_origin_non_promo))
                    ),
                )

        # testing flag_after_promo values
        df_dummy_2 = pd.DataFrame()
        for promo in promo_cols:
            df_dummy_2 = pd.concat(
                [
                    df_dummy_2,
                    df_dummy.loc[df_dummy[promo] != 1],
                ]
            )

        # generate flag_after_promo
        after_promo_list = []
        for promo in promo_cols:
            for y in [
                list(x)
                for x in zip(
                    df_dummy.loc[
                        (df_dummy[promo].diff() == -1)
                        & (df_dummy["id"].shift(1) == df_dummy["id"])
                    ].id,
                    df_dummy.loc[
                        (df_dummy[promo].diff() == -1)
                        & (df_dummy["id"].shift(1) == df_dummy["id"])
                    ].d,
                )
            ]:
                after_promo_list.append(y)

        if len(after_promo_list) > 0:
            y = {}
            for i in range(len(after_promo_list) * 28):
                z = {}
                z["id"] = after_promo_list[i // 28][0]
                z["d"] = after_promo_list[i // 28][1] + i % 28
                y[i] = z
            after_promo = pd.DataFrame.from_dict(data=y, orient="index")
            after_promo["flag_after_promo"] = 1
            after_promo = after_promo.drop_duplicates()

            # remove after promo duration if 28 days has passed
            temp = after_promo.groupby("id", as_index=False)["d"].max()
            id_list = temp.loc[temp["d"] > self.min_delivery_d]["id"].unique().tolist()
            after_promo = after_promo.loc[after_promo["id"].isin(id_list)].reset_index(
                drop=True
            )
            del temp

        # merge with dummy data
        df_dummy_2 = df_dummy_2.merge(after_promo, on=["id", "d"], how="left")
        df_dummy = df_dummy.merge(df_dummy_2, on=["id", "d"], how="left")
        df_dummy["flag_after_promo"] = (
            df_dummy["flag_after_promo"].fillna(0).astype(np.int8)
        )

        # test flag_after_promo values
        diff_rows = np.where(
            df_dummy.loc[
                (df_dummy["d"] > 20), ["id", "d", "flag_after_promo"]
            ].reset_index(drop=True)
            != df_result[["id", "d", "flag_after_promo"]]
            .sort_values(by=["id", "d"])
            .reset_index(drop=True)
        )[0]
        assert len(diff_rows) == 0

        # testing dataframe shape
        assert df_result.shape[0] == len(self.sales.loc[self.sales["d"] > 20])
        assert df_result.shape[1] == self.sales.shape[1] + 1 + (
            len(feature_param.VALUE) * 2
        )  # flag_after_promo + (Lag + EMA) * param
