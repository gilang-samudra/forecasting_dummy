import csv
import datetime
import logging
import os

import pandas as pd
import pytest
from src.inference.inference import DayOfWeekAverage
from src.utils import filter_id


@pytest.fixture()
def dummy_dow():
    windows = [
        datetime.datetime(2021, 9, 11, 0, 0),
        datetime.datetime(2022, 9, 17, 0, 0),
        datetime.datetime(2022, 9, 18, 0, 0),
        datetime.datetime(2022, 9, 28, 0, 0),
    ]

    preprocessed_sales = pd.read_csv("test/data/general/preprocessed_sales.csv")
    with open("test/data/inference/date_map.csv") as csv_file:
        reader = csv.reader(csv_file)
        date_map = dict(reader)

    date_map = {k: int(v) for k, v in date_map.items()}

    filtered = filter_id(df=preprocessed_sales, col="dc_code", selected=["1JZ1"])

    day_of_week_average = DayOfWeekAverage(
        df=filtered, windows=windows, date_map=date_map
    )

    logging.info("Execute day of week average fixture")
    return day_of_week_average


class TestDayOfWeekAverage:
    def test_compute(self, dummy_dow):
        dummy_dow.compute()
        submission_d28 = dummy_dow.result

        expected_submission_d28 = pd.read_csv(
            "test/data/inference/expected_submission_d28.csv"
        )

        assert submission_d28.equals(expected_submission_d28)
