import numpy as np
import pandas as pd
from src.inference.store_sales_type import infer_store_type, mann_whitney_inference


def test_infer_store_type():
    sales_df = pd.read_csv("test/data/general/sales_training_data.csv")
    sales_df = sales_df.drop(["customer_id"], axis=1)
    delivery_group = pd.read_csv("test/data/general/delivery_group_rev_20221024.csv")

    outlet_list = delivery_group["outlet_code"].tolist()
    store_type = infer_store_type(df=sales_df, outlet_list=outlet_list)

    expected_store_type = pd.read_csv("test/data/inference/expected_store_type.csv")
    assert store_type.equals(expected_store_type)


def test_mann_whitney_inference():
    day_sales_1 = np.array([3, 0, 0, 0, 0])
    end_sales_1 = np.array([3, 0, 0, 0, 0])

    day_sales_2 = np.array([10, 10, 1, 10, 10])
    end_sales_2 = np.array([3, 0, 0, 0, 0])

    day_sales_3 = np.array([3, 0, 0, 0, 0])
    end_sales_3 = np.array([10, 0, 0, 0, 0])

    day_sales_4 = np.array([3, 0, 0, 0, 0])
    end_sales_4 = np.array([10, 10, 1, 10, 10])

    assert mann_whitney_inference(day_sales_1, end_sales_1) == "normal"
    assert mann_whitney_inference(day_sales_2, end_sales_2) == "weekday"
    assert mann_whitney_inference(day_sales_3, end_sales_3) == "normal"
    assert mann_whitney_inference(day_sales_4, end_sales_4) == "weekend"
