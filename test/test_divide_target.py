import pandas as pd
import pytest

import src.so_calculation.divide_target as divide_target


class TestDivideTarget:
    @pytest.fixture(autouse=True)
    def _add_dummy(self) -> None:
        # load and generate required data
        self.forecast = pd.read_csv(
            "test/data/so_calculation/divide_target/forecast_20221031.csv"
        )
        self.forecast[["dc_code", "outlet_code", "product_plu"]] = self.forecast[
            "id"
        ].str.split("_", expand=True)
        self.target = pd.read_csv(
            "test/data/so_calculation/divide_target/param_target_sample.csv"
        )
        self.product_price = pd.read_csv(
            "test/data/so_calculation/divide_target/product_price.csv"
        )
        self.product_price["product_plu"] = self.product_price["product_plu"].astype(
            str
        )
        self.new_products = {
            "2022-11-03": ["660039", "660042"],
            "2022-11-04": ["660042"],
            "2022-11-14": [],
        }

        # load the correct result
        self.result_daily_target = pd.read_pickle(
            "test/data/so_calculation/divide_target/divide_target_daily.pkl"
        )
        self.result_proportion = pd.read_pickle(
            "test/data/so_calculation/divide_target/divide_target_proportion.pkl"
        )
        self.result_final_price = pd.read_pickle(
            "test/data/so_calculation/divide_target/divide_target_final_price.pkl"
        )

    def test_allocate_daily_target(self) -> None:
        so_daily_target = divide_target.allocate_daily_target(
            forecast=self.forecast,
            target=self.target,
            price=self.product_price,
            new_products=self.new_products,
        )

        assert so_daily_target.shape[1] == 22
        assert so_daily_target.isna().sum().sum() == 0
        assert len(so_daily_target.compare(self.result_daily_target)) == 0

    def test_get_proportion(self) -> None:
        proportion, final_price = divide_target.get_proportion(
            forecast=self.forecast, price=self.product_price
        )

        assert proportion.isna().sum().sum() == final_price.isna().sum().sum() == 0
        assert len(proportion.compare(self.result_proportion)) == 0
        assert len(final_price.compare(self.result_final_price)) == 0
