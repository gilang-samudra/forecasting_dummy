import datetime
import os
from typing import List

import pandas as pd
from mock import patch
from src import main, utils
from src.feature_engineering import feature_bottom_up


def mock_get_param_from_db() -> List[pd.DataFrame]:
    approach = pd.read_csv("test/data/general/approach.csv")
    return_risk = pd.read_csv("test/data/general/return_risk.csv")
    product_price = pd.read_csv("test/data/general/product_price.csv")
    release_info = pd.read_csv("test/data/general/release_info.csv")
    return approach, return_risk, product_price, release_info


def mock_utils_download(*file_name: str) -> bool:
    return os.path.exists(file_name)


def mock_files_path() -> List[str]:
    BASE_PATH = DOWNLOAD_PATH = SUBMISSION_PATH = "test/data/general"

    return BASE_PATH, DOWNLOAD_PATH, SUBMISSION_PATH


def mock_save_and_upload(
    today_date: datetime.datetime,
    submission_path: str,
    store_type: pd.DataFrame,
    trend: pd.DataFrame,
    final_so: pd.DataFrame,
) -> None:
    store_type.to_csv("test/data/main/output_store_type.csv", index=False)
    trend.to_csv("test/data/main/output_trend.csv", index=False)
    final_so.to_csv("test/data/main/output_final_so.csv", index=False)


@patch.object(utils, "download")
@patch.object(utils, "make_directory")
@patch.object(main, "save_and_upload")
@patch.object(main, "today_func")
@patch.object(main, "get_param_from_db")
@patch.object(main, "files_path")
def test_main(files_path, get_param, time, upload, make_dir, utils_download):

    files_path.return_value = mock_files_path()
    get_param.return_value = mock_get_param_from_db()
    time.return_value = datetime.datetime(2022, 10, 25, 0, 0)
    upload.side_effect = mock_save_and_upload
    make_dir.return_value = True
    utils_download.return_value = mock_utils_download
    main.main()
    store_type = pd.read_csv("test/data/main/output_store_type.csv")
    trend = pd.read_csv("test/data/main/output_trend.csv")
    final_so = pd.read_csv("test/data/main/output_final_so.csv")
    expected_so = pd.read_pickle("test/data/main/expected_final_so.pkl")

    for temp_df in [store_type, trend, final_so]:
        assert temp_df.isna().sum().sum() == 0
    assert final_so.equals(expected_so)
