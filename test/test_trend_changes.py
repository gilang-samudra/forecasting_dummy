import pandas as pd
import datetime
from src.inference import trend_changes


def test_last_year_date():
    start = datetime.datetime(2021, 9, 11)
    end = datetime.datetime(2021, 9, 17)

    date_list = trend_changes.last_year_date(start, end)
    expected_date_list = [
        "2020-09-12",
        "2020-09-13",
        "2020-09-14",
        "2020-09-15",
        "2020-09-16",
        "2020-09-17",
        "2020-09-18",
    ]

    assert date_list == expected_date_list


def test_last_year_date_leap_day():
    start = datetime.datetime(2020, 2, 29)
    end = datetime.datetime(2020, 3, 5)

    date_list = trend_changes.last_year_date(start, end)
    expected_date_list = [
        "2019-03-02",
        "2019-03-03",
        "2019-03-04",
        "2019-03-05",
        "2019-03-06",
        "2019-03-07",
    ]

    assert date_list == expected_date_list


def test_last_month_date_case1():
    start = datetime.datetime(2020, 3, 27)
    end = datetime.datetime(2020, 3, 30)

    date_list = trend_changes.last_month_date(start, end)
    expected_date_list = [
        "2020-02-28",
        "2020-02-29",
        "2020-03-01",
        "2020-03-02",
    ]

    assert date_list == expected_date_list


def test_last_month_date_case2():
    start = datetime.datetime(2020, 3, 30)
    end = datetime.datetime(2020, 4, 4)

    date_list = trend_changes.last_month_date(start, end)
    expected_date_list = [
        "2020-03-02",
        "2020-03-03",
        "2020-03-04",
        "2020-03-05",
        "2020-03-06",
        "2020-03-07",
    ]

    assert date_list == expected_date_list


def test_last_week_date_case1():
    start = datetime.datetime(2022, 3, 25)
    end = datetime.datetime(2022, 4, 4)
    sales_df = pd.read_csv("test/data/general/sales_training_data.csv")

    date_list = trend_changes.last_week_date(start, end, sales_df.columns)
    expected_date_list = [
        "2022-03-18",
        "2022-03-19",
        "2022-03-20",
        "2022-03-21",
        "2022-03-22",
        "2022-03-23",
        "2022-03-24",
        "2022-03-25",
        "2022-03-26",
        "2022-03-27",
        "2022-03-28",
    ]

    assert date_list == expected_date_list


def test_last_week_date_case2():
    start = datetime.datetime(2020, 3, 25)
    end = datetime.datetime(2020, 4, 4)
    sales_df = pd.read_csv("test/data/general/sales_training_data.csv")

    date_list = trend_changes.last_week_date(start, end, sales_df.columns)
    expected_date_list = [
        "2020-03-11",
        "2020-03-12",
        "2020-03-13",
        "2020-03-14",
        "2020-03-15",
        "2020-03-16",
        "2020-03-17",
        "2020-03-18",
        "2020-03-19",
        "2020-03-20",
        "2020-03-21",
    ]

    assert date_list == expected_date_list


def test_trend_changes():
    trend_input = pd.read_csv("test/data/inference/reference_data.csv", nrows=1000)
    sales_filtered = pd.read_csv("test/data/inference/sales_data_actual.csv")
    expected_trend = pd.read_pickle("test/data/inference/expected_trend.pkl")

    trend = trend_changes.trend_changes(
        reference_data=trend_input,
        sales_data_actual=sales_filtered,
    )

    assert trend.equals(expected_trend)
