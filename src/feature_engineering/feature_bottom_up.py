import numpy as np
import pandas as pd
import polars as pl
from src.config import FeatureParam
from typing import List

feature_param = FeatureParam()


def label_encoding(df: pd.DataFrame) -> pd.DataFrame:
    """Perform label encoding on data and create map to original label."""

    # Grouped Time Series
    outlets = df.outlet_code.cat.codes.unique().tolist()
    conv_id = dict(zip(df.id.cat.codes, df.id))

    # Label Encoding
    cols = df.dtypes.index.tolist()
    types = df.dtypes.values.tolist()

    for i, data_type in enumerate(types):
        if data_type.name == "category":
            df[cols[i]] = df[cols[i]].cat.codes

    return outlets, conv_id, df


def mean_encoding(df: pd.DataFrame) -> pd.DataFrame:
    # Mean Encoding features
    df.sort_values(by=["id", "d"], ignore_index=True, inplace=True)

    df["id_avg_sales"] = df.groupby("id")["sales"].transform("mean").astype(np.float32)
    df["dc_code_avg_sales"] = (
        df.groupby("dc_code")["sales"].transform("mean").astype(np.float32)
    )
    df["cluster_id_avg_sales"] = (
        df.groupby("cluster_id")["sales"].transform("mean").astype(np.float32)
    )
    df["outlet_code_avg_sales"] = (
        df.groupby("outlet_code")["sales"].transform("mean").astype(np.float32)
    )
    df["category_id_avg_sales"] = (
        df.groupby("category_id")["sales"].transform("mean").astype(np.float32)
    )
    df["sub_category_id_avg_sales"] = (
        df.groupby("sub_category_id")["sales"].transform("mean").astype(np.float32)
    )
    df["product_plu_avg_sales"] = (
        df.groupby("product_plu")["sales"].transform("mean").astype(np.float32)
    )
    return df


def sales_feature_engineering(df: pd.DataFrame, min_delivery_d: int) -> pd.DataFrame:
    """Feature engineering on sales, lag, and exponential weighted mean."""

    # get promo cols
    promo_cols = [x for x in df.columns.tolist() if x.startswith("promo_")]
    df = df.sort_values(by=["id", "d"], ignore_index=True)

    # get id and d for first day after promotion period
    df = pl.from_pandas(df)

    after_promo_list = []
    for promo in promo_cols:
        for y in [
            list(x)
            for x in zip(
                df.filter(
                    (pl.col(promo).diff() == -1)
                    & (pl.col("id").shift(1) == pl.col("id"))
                )
                .select(pl.col("id"))
                .to_series(),
                df.filter(
                    (pl.col(promo).diff() == -1)
                    & (pl.col("id").shift(1) == pl.col("id"))
                )
                .select(pl.col("d"))
                .to_series(),
            )
        ]:
            after_promo_list.append(y)

    if len(after_promo_list) > 0:
        # construct dictionary for DataFrame for 28 days after promotion period
        y = {}
        for i in range(len(after_promo_list) * 28):
            z = {}
            z["id"] = after_promo_list[i // 28][0]
            z["d"] = after_promo_list[i // 28][1] + i % 28
            y[i] = z

        after_promo = pl.from_pandas(pd.DataFrame.from_dict(y, orient="index"))
        after_promo = after_promo.with_column(pl.lit(1).alias("flag_after_promo"))
        after_promo = after_promo.unique()
        after_promo = after_promo.with_columns(
            [
                pl.col("id").cast(pl.Int32, strict=True),
                pl.col("d").cast(pl.Int16, strict=True),
            ]
        )

        # remove after promo duration if 28 days has passed
        temp = after_promo.groupby(["id"]).agg(pl.col("d").max())
        id_list = (
            temp.filter(pl.col("d") > min_delivery_d)
            .select(pl.col("id"))
            .unique()
            .to_series()
            .to_list()
        )
        after_promo = after_promo.filter(pl.col("id").is_in(id_list))

        del temp

    elif len(after_promo_list) == 0:
        after_promo = pl.DataFrame(columns=["id", "d", "flag_after_promo"])
        after_promo = after_promo.with_columns(
            [
                pl.col("id").cast(pl.Int32, strict=True),
                pl.col("d").cast(pl.Int16, strict=True),
                pl.col("flag_after_promo").cast(pl.Int32, strict=True),
            ]
        )

    # * for Non Promo duration
    # normal feature engineering
    temp_non_promo = pl.DataFrame()
    temp_promo = pl.DataFrame()
    for promo in promo_cols:
        temp_non_promo = pl.concat([temp_non_promo, df.filter(pl.col(promo) != 1)])

        temp_promo = pl.concat([temp_promo, df.filter(pl.col(promo) == 1)])

    del df

    # Lag and EMA features
    for n in feature_param.VALUE:
        temp_non_promo = temp_non_promo.with_column(
            pl.col("sales")
            .shift(n)
            .over(["outlet_code", "product_plu"])
            .cast(pl.Float32)
            .alias(f"SALES_LAG_{n}")
        )

        temp_non_promo = temp_non_promo.with_column(
            pl.col("sales")
            .shift(n)
            .ewm_mean(com=12)
            .over(["outlet_code", "product_plu"])
            .cast(pl.Float32)
            .alias(f"SALES_EMA_{n}")
        )

        # * for Promo duration
        # get max value of lag and EMA
        max_lags = temp_non_promo.groupby(["outlet_code", "product_plu"]).agg(
            pl.col(f"SALES_LAG_{n}").max()
        )
        temp_promo = temp_promo.join(
            max_lags, on=["outlet_code", "product_plu"], how="left"
        )

        max_shifts = temp_non_promo.groupby(["outlet_code", "product_plu"]).agg(
            pl.col(f"SALES_EMA_{n}").max()
        )
        temp_promo = temp_promo.join(
            max_shifts, on=["outlet_code", "product_plu"], how="left"
        )

    # * for After Promo duration
    # get 0.5 * normal feature engineering value
    temp_non_promo = temp_non_promo.with_columns(
        [
            pl.col("id").cast(pl.Int32, strict=True),
            pl.col("d").cast(pl.Int16, strict=True),
        ]
    )
    temp_non_promo = temp_non_promo.join(after_promo, on=["id", "d"], how="left")
    temp_non_promo = temp_non_promo.with_column(pl.col("flag_after_promo").fill_null(0))

    # Lag and EMA features
    for n in feature_param.VALUE:
        temp_non_promo = temp_non_promo.with_column(
            pl.when(pl.col("flag_after_promo") == 1)
            .then((pl.col(f"SALES_LAG_{n}") * 0.5).cast(pl.Float32))
            .otherwise(pl.col(f"SALES_LAG_{n}").cast(pl.Float32))
        )

        temp_non_promo = temp_non_promo.with_column(
            pl.when(pl.col("flag_after_promo") == 1)
            .then((pl.col(f"SALES_EMA_{n}") * 0.5).cast(pl.Float32))
            .otherwise(pl.col(f"SALES_EMA_{n}").cast(pl.Float32))
        )

    # concatenate
    temp_promo = temp_promo.with_columns(
        [
            pl.col("id").cast(pl.Int32),
            pl.col("d").cast(pl.Int16),
            pl.col("SALES_LAG_7").cast(pl.Float32),
            pl.col("SALES_LAG_13").cast(pl.Float32),
            pl.col("SALES_LAG_20").cast(pl.Float32),
            pl.lit(0).cast(pl.Int32).alias("flag_after_promo"),
        ]
    )

    temp_non_promo.vstack(temp_promo, in_place=True)

    del temp_promo
    temp_non_promo = temp_non_promo.filter(pl.col("d") > 20)

    return temp_non_promo
