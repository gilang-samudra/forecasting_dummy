from typing import Dict

import numpy as np
import pandas as pd
from src.config import OutputColumns

from src.so_calculation.calculate_so import CalculationHelper, QuantityAdjustment

output_columns = OutputColumns()


def get_proportion(forecast: pd.DataFrame, price: pd.DataFrame) -> pd.DataFrame:
    """Calculates amount proportion from forecasted sales"""

    df = forecast.copy()
    df["dc_code"] = df["id"].apply(lambda x: x.split("_")[0])
    df["outlet_code"] = df["id"].apply(lambda x: x.split("_")[1])
    df["product_plu"] = df["id"].apply(lambda x: x.split("_")[2])

    price["product_plu"] = price["product_plu"].astype(str)
    try:
        price["start_at"] = price["start_at"].apply(lambda x: x.strftime("%Y-%m-%d"))
    except:
        pass

    # get all prices before delivery date
    delivery_dates = forecast["delivery_date"].unique().tolist()
    reg_price = price.loc[~price["start_at"].isin(delivery_dates)]
    idx = (
        reg_price.groupby(["dc_code", "product_plu"])["start_at"].transform(max)
        == reg_price["start_at"]
    )
    reg_price = reg_price[idx]
    reg_price = reg_price.drop(["start_at"], axis=1)
    df = pd.merge(df, reg_price, on=["dc_code", "product_plu"], how="left")

    # check for price changes coinciding with delivery date
    price_intersect = set(price["start_at"].unique().tolist()).intersection(
        set(delivery_dates)
    )
    if len(price_intersect) > 0:
        updated_price = pd.DataFrame()
        # get price that starts on delivery date
        for d in price_intersect:
            temp = price.loc[price["start_at"] == d]
            updated_price = pd.concat([updated_price, temp], ignore_index=True)
        # update old price with new price
        updated_price = updated_price.rename({"start_at": "delivery_date"}, axis=1)
        df = df.set_index(["dc_code", "product_plu", "delivery_date"])
        updated_price = updated_price.set_index(
            ["dc_code", "product_plu", "delivery_date"]
        )
        df.update(updated_price)
        df = df.reset_index()

        del updated_price

    del reg_price

    final_price = (
        df[["dc_code", "product_plu", "price", "delivery_date"]]
        .copy()
        .drop_duplicates()
    )

    # convert quantity to amount
    df["expected_sales_amt"] = df["expected_sales"] * df["price"]
    df = df[
        [
            "id",
            "dc_code",
            "outlet_code",
            "product_plu",
            "outlet_group",
            "delivery_date",
            "expected_sales_amt",
        ]
    ]

    # get sum of amount at each DC
    df["sum_expected_sales_amt"] = df.groupby(["dc_code", "delivery_date"])[
        "expected_sales_amt"
    ].transform(sum)

    # get amount proportion per product per outlet
    df["proportion"] = df["expected_sales_amt"] / df["sum_expected_sales_amt"]

    df = df[["id", "outlet_group", "proportion"]]

    return df, final_price


def allocate_daily_target(
    forecast: pd.DataFrame,
    target: pd.DataFrame,
    price: pd.DataFrame,
    new_products: Dict[str, str],
) -> pd.DataFrame:
    """Allocates daily target amount to quantity per product per outlet"""

    global quantity_adjustment
    quantity_adjustment = QuantityAdjustment(new_products=new_products)

    # get proportion, target amount, and price
    proportion, final_price = get_proportion(forecast=forecast, price=price)
    target = target.rename({"date": "delivery_date"}, axis=1)

    # get ID and expected sales from BU forecast
    df = forecast.copy()
    temp = df[["id", "outlet_group", "delivery_date", "expected_sales"]].copy()
    temp["dc_code"] = temp["id"].apply(lambda x: x.split("_")[0])
    temp["product_plu"] = temp["id"].apply(lambda x: x.split("_")[2])

    # merging
    temp = pd.merge(
        temp, final_price, on=["dc_code", "product_plu", "delivery_date"], how="left"
    )
    temp = pd.merge(temp, target, on=["delivery_date", "dc_code"], how="left")
    temp = pd.merge(temp, proportion, on=["id", "outlet_group"], how="left")

    # calculate proportional SO amount and convert to quantity
    temp["SO_amount"] = temp["amount"] * temp["proportion"]
    temp["SO"] = np.round(temp["SO_amount"] / temp["price"], decimals=0)

    # update forecast
    temp = temp.set_index(["id", "outlet_group"])
    df = df.set_index(["id", "outlet_group"])
    df.update(temp)
    df = df.reset_index()

    del temp

    df = df[output_columns.FORECAST]

    # adjust quantity
    try:
        df["delivery_date"] = df["delivery_date"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )
    except:
        pass
    delivery_dates = df["delivery_date"].unique().tolist()
    for ddate in delivery_dates:
        df.loc[df["delivery_date"] == ddate, "SO"] = df.loc[
            df["delivery_date"] == ddate
        ].apply(
            lambda x: 0 if x.id.split("_")[2] in new_products[ddate] else x.SO,
            axis=1,
        )
    df = quantity_adjustment.min_qty_adjustment(df=df)
    df = quantity_adjustment.min_new_adjustment(df=df)

    df["SO"] = df["SO"].apply(lambda x: np.round(x, decimals=0))

    # enforce output columns
    df = df[output_columns.FORECAST]

    df["sales_ratio"] = df.apply(
        lambda x: CalculationHelper.calculate_sales_ratio(
            x.expected_sales, x.SO, x.est_remaining_stock
        ),
        axis=1,
    )

    return df
