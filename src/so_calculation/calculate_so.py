import datetime
import logging
import re
from typing import Any, Dict, List, Tuple

import numpy as np
import pandas as pd
import pulp as p
import src.utils_light_version as utils
from src.config import OutputColumns, ScheduleParam, SOParam

so_param = SOParam()
schedule_param = ScheduleParam()
output_columns = OutputColumns()


class ElementHelper:
    """Helper functions for SO calculation. Contains:
    - `day_distance`: Calculate distance between days of week with d2 ahead of d1.
    - `calculate_part`: Calculate value for a new column using specified columns and sum formula. \n
    (* = need to instantiate class)"""

    @staticmethod
    def day_distance(d1: str, d2: str) -> int:
        """Calculate distance between days of week with d2 ahead of d1."""
        if d2 == d1:
            distance = 7
        else:
            distance = (
                schedule_param.DAYSOFWEEK.index(d2)
                - schedule_param.DAYSOFWEEK.index(d1)
            ) % len(schedule_param.DAYSOFWEEK)
        return distance

    @staticmethod
    def calculate_part(
        df: pd.DataFrame,
        target: str,
        mode: str,
        **kwargs,
    ) -> None:
        """Calculate value for new column using specified columns and sum formula.\n
        Arguments:
        - df
        - target: new column name
        - mode: sum formula
        - (optional) range_list: list of lists of dates to be calculated
        - (optional) first_col: starting date column name
        - (optional) second_col: ending date column name"""
        kwargs.setdefault("datelist_type", "single")

        # NumPy Helper Functions
        def half_start(a, cols, idx):
            if len(a[idx]) > 1:
                return 0.5 * a[cols.index(a[idx][0])] + np.sum(
                    a[cols.index(a[idx][1]) : cols.index(a[idx][-1]) + 1]
                )
            else:
                return 0.5 * a[cols.index(a[idx][0])]

        def half_end(a, cols, idx):
            return (
                np.sum(a[cols.index(a[idx][0]) : cols.index(a[idx][-1])])
                + 0.5 * a[cols.index(a[idx][-1])]
            )

        def half_start_half_end(a, cols, idx):
            return (
                0.5 * a[cols.index(a[idx][0])]
                + np.sum(a[cols.index(a[idx][1]) : cols.index(a[idx][-1])])
                + 0.5 * a[cols.index(a[idx][-1])]
            )

        def full_sum(a, cols, idx):
            return np.sum(a[cols.index(a[idx][0]) : cols.index(a[idx][-1]) + 1])

        if len(df) > 0:
            if kwargs["datelist_type"] == "multiple":
                range_list = kwargs["range_list"]
            else:
                range_list = ScheduleHelper.generate_date_list(
                    df, kwargs["first_col"], kwargs["second_col"]
                )

            df["range_list"] = range_list
            date_cols = [
                x for x in df.columns if re.match(r".*[0-9]{4}-[0-9]{2}-[0-9]{2}", x)
            ]
            df_cols = ["id", "range_list"] + date_cols
            df_arr = df[df_cols].to_numpy()

            idx = df_cols.index("range_list")
            if mode == "half_start":
                df[target] = np.apply_along_axis(
                    func1d=half_start, axis=1, arr=df_arr, cols=df_cols, idx=idx
                )
            elif mode == "half_end":
                df[target] = np.apply_along_axis(
                    func1d=half_end, axis=1, arr=df_arr, cols=df_cols, idx=idx
                )
            elif mode == "half_start_half_end":
                df[target] = np.apply_along_axis(
                    func1d=half_start_half_end,
                    axis=1,
                    arr=df_arr,
                    cols=df_cols,
                    idx=idx,
                )
            else:
                df[target] = np.apply_along_axis(
                    func1d=full_sum, axis=1, arr=df_arr, cols=df_cols, idx=idx
                )

            df = df.drop(["range_list"], axis=1)

            del df_arr, df_cols, idx


class ReturnRiskAdjustment:
    """Adjustment policy for return risk. Contains:
    - `weekday_weekend_adjustment`: Add adjustment percentage based on outlet sales type.
    - `payday_adjustment`: Add adjustment percentage based on payday period. \n
    (* = need to instantiate class)"""

    @staticmethod
    def weekday_weekend_adjustment(df: pd.DataFrame) -> None:
        """Add adjustment percentage based on outlet sales type."""

        # adjustment for normal stores
        df.loc[df["store_type"] == "normal", "store_type_adjustment"] = 0

        # adjustment for group C weekday stores
        df.loc[
            df["outlet_group"].str.startswith("C")
            & (df["store_type"] == "weekday")
            & (df["delivery_day"].isin(schedule_param.WEEKDAY_DELIVERY_C)),
            "store_type_adjustment",
        ] = so_param.PCT_MAX_GROUP_C_WDAY

        df.loc[
            df["outlet_group"].str.startswith("C")
            & (df["store_type"] == "weekday")
            & (df["delivery_day"].isin(schedule_param.WEEKEND_DELIVERY_C)),
            "store_type_adjustment",
        ] = so_param.PCT_MIN_GROUP_C_WDAY

        # adjustment for group C weekend stores
        df.loc[
            df["outlet_group"].str.startswith("C")
            & (df["store_type"] == "weekend")
            & (df["delivery_day"].isin(schedule_param.WEEKDAY_DELIVERY_C)),
            "store_type_adjustment",
        ] = so_param.PCT_MIN_GROUP_C_WEND

        df.loc[
            df["outlet_group"].str.startswith("C")
            & (df["store_type"] == "weekend")
            & (df["delivery_day"].isin(schedule_param.WEEKEND_DELIVERY_C)),
            "store_type_adjustment",
        ] = so_param.PCT_MAX_GROUP_C_WEND

        # adjustment for group E weekday stores
        df.loc[
            df["outlet_group"].str.startswith("E")
            & (df["store_type"] == "weekday")
            & (df["delivery_day"].isin(schedule_param.WEEKDAY_DELIVERY_E)),
            "store_type_adjustment",
        ] = so_param.PCT_MAX_GROUP_E

        df.loc[
            df["outlet_group"].str.startswith("E")
            & (df["store_type"] == "weekday")
            & (df["delivery_day"].isin(schedule_param.WEEKEND_DELIVERY_E)),
            "store_type_adjustment",
        ] = so_param.PCT_MIN_GROUP_E

        # adjustment for group E weekday stores
        df.loc[
            df["outlet_group"].str.startswith("E")
            & (df["store_type"] == "weekend")
            & (df["delivery_day"].isin(schedule_param.WEEKDAY_DELIVERY_E)),
            "store_type_adjustment",
        ] = so_param.PCT_MIN_GROUP_E

        df.loc[
            df["outlet_group"].str.startswith("E")
            & (df["store_type"] == "weekend")
            & (df["delivery_day"].isin(schedule_param.WEEKEND_DELIVERY_E)),
            "store_type_adjustment",
        ] = so_param.PCT_MAX_GROUP_E

    @staticmethod
    def payday_adjustment(df: pd.DataFrame) -> None:
        """Add adjustment percentage based on payday period."""

        df.loc[
            df["delivery_day"].isin(schedule_param.PAYDAY_PERIOD), "payday_adjustment"
        ] = so_param.PCT_PAYDAY
        df.loc[
            ~df["delivery_day"].isin(schedule_param.PAYDAY_PERIOD), "payday_adjustment"
        ] = so_param.PCT_NONPAYDAY


class QuantityAdjustment:
    """Adjustment policy for SO quantity. Contains:
    - *`lp_for_qty`: Distribute SO using linear programming for fulfilling the minimum required quantity SO.
    - *`min_qty_adjustment`: Adjust delivery qty to fulfill minimum of 8 pcs/delivery.
    - *`min_new_adjustment`: Adjust delivery qty for new products. \n
    (* = need to instantiate class)"""

    def __init__(self, new_products: List[str]) -> None:
        self.new_products = new_products

    def lp_for_qty(self, df: pd.DataFrame) -> pd.DataFrame:
        """Distribute SO using linear programming for fulfilling the minimum required quantity SO."""
        prop_list = dict(df.loc[:, ["product_plu", "weight"]].values)

        dec_vars = p.LpVariable.dicts("", df.product_plu.values, None, None, "Float")
        prob = p.LpProblem("Min_Quantity", p.LpMaximize)

        # Creates the objective function
        prob += p.lpSum([dec_vars[a] for a in df.product_plu.values])

        # Create constraints
        prob += p.lpSum([dec_vars[a] for a in df.product_plu.values]) <= 8

        for n in df.product_plu.values:
            prob += dec_vars[n] >= prop_list[n] * p.lpSum(
                dec_vars[a] for a in df.product_plu.values
            )

        prob.solve(p.PULP_CBC_CMD(msg=False))

        prod_sum = 0
        for v in prob.variables():
            prod_sum += round(v.varValue, 0)
            df.loc[df["product_plu"] == v.name[1:], "SO"] = round(v.varValue, 0)

        if prod_sum < 8:
            delta = 8 - prod_sum

            # allocate all remaining quantity to '660039' otherwise select the product randomly
            if "660039" in df["product_plu"].values:
                df.loc[df["product_plu"] == "660039", "SO"] = (
                    df.loc[df["product_plu"] == "660039", "SO"] + delta
                )
            else:
                ran_prod = df.sample()["product_plu"].values[0]
                df.loc[df["product_plu"] == ran_prod, "SO"] = (
                    df.loc[df["product_plu"] == ran_prod, "SO"] + delta
                )

        return df

    def min_qty_adjustment(self, df: pd.DataFrame) -> pd.DataFrame:
        """Adjust delivery qty to fulfill minimum of 8 pcs/delivery."""

        # group by outlet
        df["outlet_code"] = df["id"].apply(lambda x: x.split("_")[1])
        df["product_plu"] = df["id"].apply(lambda x: x.split("_")[2])

        # calculate weight of SO qty
        df["sum"] = df.groupby(["outlet_code", "outlet_group"], as_index=False)[
            "SO"
        ].transform("sum")
        df["diff"] = so_param.MIN_DELIVERY_QTY - df["sum"]
        df["weight"] = df["SO"] / df["sum"]

        # adjust SO with qty below minimum
        adjust_df = df.loc[(df["sum"] < so_param.MIN_DELIVERY_QTY) & (df["sum"] > 0)][
            ["outlet_code", "outlet_group"]
        ].drop_duplicates(ignore_index=True)

        if len(adjust_df) != 0:
            adjust_list = list(zip(adjust_df["outlet_code"], adjust_df["outlet_group"]))

            adjusted = pd.DataFrame()
            for pair in adjust_list:
                # call linear programming function to distribute the SO
                temp = df.loc[
                    (df["outlet_code"] == pair[0]) & (df["outlet_group"] == pair[1])
                ].reset_index(drop=True)
                temp = self.lp_for_qty(df=temp)
                adjusted = pd.concat(
                    [
                        adjusted,
                        temp,
                    ],
                    ignore_index=True,
                )

            del temp

            # update SO qty with adjusted value
            df = df.set_index(["id", "outlet_group"])
            adjusted = adjusted.set_index(["id", "outlet_group"])
            df.update(adjusted)
            df = df.reset_index()
            adjusted = adjusted.reset_index()

            del adjusted

        del adjust_df

        df = df.drop(["outlet_code", "product_plu", "sum", "diff", "weight"], axis=1)
        df["SO"] = df["SO"].astype(int)

        return df

    def min_new_adjustment(self, df: pd.DataFrame) -> pd.DataFrame:
        """Adjust delivery qty for new products."""

        df["dc_code"] = df["id"].apply(lambda x: x.split("_")[0])
        df["outlet_code"] = df["id"].apply(lambda x: x.split("_")[1])
        df["product_plu"] = df["id"].apply(lambda x: x.split("_")[2])

        try:
            df["delivery_date"] = df["delivery_date"].apply(
                lambda x: x.strftime("%Y-%m-%d")
            )
        except:
            pass

        for delivery_date, new_products_list in self.new_products.items():
            df["sum"] = df.groupby(["outlet_code", "delivery_date"])["SO"].transform(
                "sum"
            )
            included_outlets = (
                df.loc[(df["sum"] > 0) & (df["delivery_date"] == delivery_date)][
                    "outlet_code"
                ]
                .unique()
                .tolist()
            )

            # adjust existing qty
            df.loc[
                (df["delivery_date"] == delivery_date)
                & (df["product_plu"].isin(new_products_list))
                & (df["outlet_code"].isin(included_outlets))
                & ~(
                    df["outlet_group"].str.startswith("C")
                    | df["outlet_group"].str.startswith("E")
                ),
                "SO",
            ] = so_param.NEW_QTY_OTHER
            df.loc[
                (df["delivery_date"] == delivery_date)
                & (df["product_plu"].isin(new_products_list))
                & (df["outlet_code"].isin(included_outlets))
                & (df["outlet_group"].str.startswith("C")),
                "SO",
            ] = so_param.NEW_QTY_GROUP_C
            df.loc[
                (df["delivery_date"] == delivery_date)
                & (df["product_plu"].isin(new_products_list))
                & (df["outlet_code"].isin(included_outlets))
                & (df["outlet_group"].str.startswith("E")),
                "SO",
            ] = so_param.NEW_QTY_GROUP_E

            # adjust nonexistent qty
            for product in new_products_list:
                # get outlets with nonexistent new product
                exist_outlets = (
                    df.loc[df["product_plu"] == product]["outlet_code"]
                    .unique()
                    .tolist()
                )
                nonexist_outlets = [
                    x for x in included_outlets if x not in exist_outlets
                ]

                temp = df.loc[
                    (df["outlet_code"].isin(nonexist_outlets))
                    & (df["delivery_date"] == delivery_date)
                ][
                    ["dc_code", "outlet_code", "outlet_group", "delivery_date"]
                ].reset_index(
                    drop=True
                )
                temp = temp.drop_duplicates(ignore_index=True)

                if len(temp) > 0:
                    # add minimum qty
                    temp["id"] = temp.apply(
                        lambda x: f"{x.dc_code}_{x.outlet_code}_{product}", axis=1
                    )
                    temp.loc[:, "SO"] = so_param.NEW_QTY_OTHER
                    temp.loc[
                        temp["outlet_group"].str.startswith("C"), "SO"
                    ] = so_param.NEW_QTY_GROUP_C
                    temp.loc[
                        temp["outlet_group"].str.startswith("E"), "SO"
                    ] = so_param.NEW_QTY_GROUP_E

                    df = pd.concat([df, temp], ignore_index=True)
                    df = df.fillna(0)

                del temp

        df = df.drop(["dc_code", "outlet_code", "product_plu", "sum"], axis=1)

        return df


class ScheduleHelper:
    """Schedule related functions for SO calculation. Contains:
    - `add_offset`: Add offset column of forecast day to delivery day.
    - `generate_sequential_delivery`: Add sequential delivery columns.
    - `generate_date_list`: Generate list of date ranges from specified columns.
    - *`generate_multiple_date_list`: Generate list of delivery dates and first delivery dates from specified columns. \n
    (* = need to instantiate class)"""

    @staticmethod
    def add_offset(df: pd.DataFrame, today_day: str) -> None:
        """Add offset column of forecast day to delivery day."""
        df["offset"] = 3
        if today_day == "Thu":
            df.loc[
                (df["outlet_group"] == "C1") | (df["outlet_group"] == "E1"),
                "offset",
            ] = 4
        elif today_day == "Fri":
            df["offset"] = 4
            df.loc[
                (df["outlet_group"] == "C1") | (df["outlet_group"] == "E3"),
                "offset",
            ] = 5

        df["offset"] = df["offset"].astype(np.int8)

    @staticmethod
    def generate_sequential_delivery(
        df: pd.DataFrame, today_date: datetime.datetime
    ) -> None:
        """Add sequential delivery columns."""

        df["delivery_date"] = df["offset"].apply(
            lambda x: (today_date + datetime.timedelta(x))
        )
        df["delivery_day"] = df["delivery_date"].apply(lambda x: x.strftime("%a"))

        # find previous and next delivery days
        df["prev_delivery_day"] = df.apply(
            lambda x: schedule_param.DELIVERY_SCHEDULE[x.outlet_group][
                (
                    schedule_param.DELIVERY_SCHEDULE[x.outlet_group].index(
                        x.delivery_day
                    )
                    - 1
                )
                % len(schedule_param.DELIVERY_SCHEDULE[x.outlet_group])
            ],
            axis=1,
        )
        df["prev_2_delivery_day"] = df.apply(
            lambda x: schedule_param.DELIVERY_SCHEDULE[x.outlet_group][
                (
                    schedule_param.DELIVERY_SCHEDULE[x.outlet_group].index(
                        x.delivery_day
                    )
                    - 2
                )
                % len(schedule_param.DELIVERY_SCHEDULE[x.outlet_group])
            ],
            axis=1,
        )
        df["next_delivery_day"] = df.apply(
            lambda x: schedule_param.DELIVERY_SCHEDULE[x.outlet_group][
                (
                    schedule_param.DELIVERY_SCHEDULE[x.outlet_group].index(
                        x.delivery_day
                    )
                    + 1
                )
                % len(schedule_param.DELIVERY_SCHEDULE[x.outlet_group])
            ],
            axis=1,
        )

        # calculate distance
        df["prev_distance"] = df.apply(
            lambda x: ElementHelper.day_distance(
                d1=x.prev_delivery_day, d2=x.delivery_day
            ),
            axis=1,
        )
        df["prev_2_distance"] = df.apply(
            lambda x: ElementHelper.day_distance(
                d1=x.prev_2_delivery_day, d2=x.delivery_day
            ),
            axis=1,
        )
        df["next_distance"] = df.apply(
            lambda x: ElementHelper.day_distance(
                d1=x.delivery_day, d2=x.next_delivery_day
            ),
            axis=1,
        )

        # calculate previous and next delivery dates
        df["prev_delivery_date"] = df.apply(
            lambda x: (x.delivery_date - datetime.timedelta(x.prev_distance)),
            axis=1,
        )
        df["prev_2_delivery_date"] = df.apply(
            lambda x: (x.delivery_date - datetime.timedelta(x.prev_2_distance)),
            axis=1,
        )
        df["next_delivery_date"] = df.apply(
            lambda x: (x.delivery_date + datetime.timedelta(x.next_distance)),
            axis=1,
        )

        # drop unused columns
        df = df.drop(
            [
                "prev_delivery_day",
                "prev_distance",
                "prev_2_delivery_day",
                "prev_2_distance",
                "next_delivery_day",
                "next_distance",
            ],
            axis=1,
        )

    def generate_date_list(
        df: pd.DataFrame, first_col: str, second_col: str
    ) -> List[List[str]]:
        """Generate list of date ranges from specified columns."""

        start_list = df[first_col].values.tolist()
        end_list = df[second_col].values.tolist()
        range_list = []

        if len(start_list) == len(end_list):
            for i in range(len(start_list)):
                temp = pd.date_range(start=start_list[i], end=end_list[i])
                temp = [x.strftime("%Y-%m-%d") for x in temp]
                range_list.append(temp)
        else:
            logging.exception("Length must match between start_list and end_list!")

        return range_list

    def generate_multiple_date_list(
        self,
        df: pd.DataFrame,
        first_col: str,
        second_col: str,
    ) -> Tuple[List[List[str]], List[str]]:
        """Generate list of delivery dates and first delivery dates from specified columns."""

        range_list = self.generate_date_list(df, first_col, second_col)
        temp_list = []
        # create dict with key = date, value = day of week
        for entry in range_list:
            temp_dict = {}
            for item in entry:
                x = datetime.datetime.strptime(item, "%Y-%m-%d")
                temp_dict[item] = x.strftime("%a")

            temp_list.append(temp_dict)

        # filter out dates with no delivery
        for i, entry in enumerate(temp_list):
            for key, value in entry.copy().items():
                if (
                    value
                    not in schedule_param.DELIVERY_SCHEDULE[df.loc[i]["outlet_group"]]
                ):
                    del entry[key]

        # create list of delivery date schedules and remove last element
        delivery_date_schedule = []
        for entry in temp_list:
            key_list = list(entry.keys())
            delivery_date_schedule.append(key_list[:-1])

        # create list of first delivery dates
        first_date = []
        for entry in range_list:
            first_date.append(entry[0])

        return delivery_date_schedule, first_date


class CalculationHelper:
    """Calculation related functions for SO calculation. Contains:
    - `shelf_life_validation`: Split data to no, single, and multiple overlap based on shelf life.
    - `limit_remaining_stock`: Exclude remaining stock for low sales products and cap remaining stock at 10 pcs.
    - `single_final_so_qty`: Calculate SO qty for no and single overlap.
    - `multiple_final_so_qty`: Calculate SO qty for multiple overlap.
    - `calculate_sales_ratio`: Calculate sales ratio from expected sales, SO qty, and estimated remaining stock. \n
    (* = need to instantiate class)"""

    @staticmethod
    def shelf_life_validation(
        df: pd.DataFrame,
    ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
        """Split data to no, single, and multiple overlap based on shelf life."""
        df = df.copy()
        df["shelf_life"] = df["shelf_life"].fillna(so_param.DEFAULT_SHELF_LIFE)

        df["next_valid"] = df.apply(
            lambda x: x.delivery_date + datetime.timedelta(x.shelf_life - 1),
            axis=1,
        )

        df["last_valid"] = df.apply(
            lambda x: x.prev_delivery_date + datetime.timedelta(x.shelf_life - 1),
            axis=1,
        )

        df["last_2_valid"] = df.apply(
            lambda x: x.prev_2_delivery_date + datetime.timedelta(x.shelf_life - 1),
            axis=1,
        )

        no_overlap = df.loc[df["last_valid"] < df["delivery_date"]].reset_index(
            drop=True
        )
        single_overlap = df.loc[
            (df["last_2_valid"] < df["delivery_date"])
            & (df["last_valid"] >= df["delivery_date"])
        ].reset_index(drop=True)
        multiple_overlap = df.loc[
            df["last_2_valid"] >= df["delivery_date"]
        ].reset_index(drop=True)

        no_overlap = no_overlap.drop(["prev_2_delivery_date", "last_2_valid"], axis=1)
        single_overlap = single_overlap.drop(
            ["prev_2_delivery_date", "last_2_valid"], axis=1
        )
        multiple_overlap = multiple_overlap.drop(
            ["prev_2_delivery_date", "last_2_valid"], axis=1
        )

        return no_overlap, single_overlap, multiple_overlap

    @staticmethod
    def limit_remaining_stock(df: pd.DataFrame) -> None:
        """Exclude remaining stock for low sales products and cap remaining stock at 10 pcs."""

        # limit for non promo period
        df.loc[
            (df["expected_sales"] <= 5) & (df["flag_after_promo"] != 1),
            "est_remaining_stock",
        ] = 0
        df.loc[
            (df["est_remaining_stock"] > 10) & (df["flag_after_promo"] != 1),
            "est_remaining_stock",
        ] = 10

        # exclude for promo period
        df.loc[df["flag_promo"] == 1, "est_remaining_stock"] = 0

    @staticmethod
    def single_final_so_qty(
        est_remaining_stock: float, overlap: float, expected_sales: float, buffer: float
    ) -> float:
        """Calculate SO qty for no and single overlap."""
        # fully fullfiled
        if est_remaining_stock >= overlap:
            so = (expected_sales * buffer) - overlap
        # partially fullfiled
        elif (est_remaining_stock > 0) & (est_remaining_stock < overlap):
            so = (expected_sales * buffer) - est_remaining_stock
        # not fullfilled
        else:
            so = expected_sales * buffer

        return so

    @staticmethod
    def multiple_final_so_qty(
        est_remaining_stock: float, expected_sales: float, buffer: float
    ) -> float:
        """Calculate SO qty for multiple overlap."""
        if (est_remaining_stock > 0) & (
            est_remaining_stock < (expected_sales * buffer)
        ):
            so = expected_sales * buffer - est_remaining_stock
        elif est_remaining_stock >= (expected_sales * buffer):
            so = 0
        else:
            so = expected_sales * buffer

        return so

    @staticmethod
    def calculate_sales_ratio(
        expected_sales: float, so_qty: float, est_remaining_stock: float
    ) -> float:
        """Calculate sales ratio from expected sales, SO qty, and estimated remaining stock."""
        if so_qty > 0:
            if est_remaining_stock >= 0:
                ratio = expected_sales / (so_qty + est_remaining_stock)
            else:
                ratio = expected_sales / so_qty
        else:
            ratio = 0

        return ratio


class SOCalculationV4:
    """Calculate SO quantity using CL_v4. Contains:
    - *`reindex_columns`: Reindex dataframe columns.
    - *`calculate_no_overlap`
    - *`calculate_single_overlap`
    - *`calculate_multiple_overlap`
    - *`calculate`: Calculate SO quantity using CL_v4 for all IDs and generate input for trend detection. \n
    (* = need to instantiate class)"""

    def __init__(
        self,
        forecast: pd.DataFrame,
        sales: pd.DataFrame,
        sales_order: pd.DataFrame,
        new_products: Dict[str, str],
        windows: List[datetime.datetime],
    ) -> None:
        self.forecast = forecast
        self.sales = sales
        self.sales_order = sales_order
        self.new_products = new_products
        (
            self.train_start,
            self.train_end,
            self.forecast_start,
            self.forecast_end,
        ) = windows

        self.QuantityAdjustment = QuantityAdjustment(new_products=self.new_products)

        # match ID
        self.forecast = utils.filter_id(
            df=self.forecast,
            col="id",
            selected=self.sales_order["id"].unique().tolist(),
        )
        self.sales_order = utils.filter_id(
            df=self.sales_order,
            col="id",
            selected=self.forecast["id"].unique().tolist(),
        )
        self.sales = utils.filter_id(
            df=self.sales, col="id", selected=self.forecast["id"].unique().tolist()
        )

        # separate based on shelf life
        (
            self.no_overlap,
            self.single_overlap,
            self.multiple_overlap,
        ) = CalculationHelper.shelf_life_validation(df=self.forecast)
        del self.forecast

    def reindex_columns(self, df: pd.DataFrame, cols: List[Any]) -> None:
        """Reindex dataframe columns."""
        try:
            df = df[cols]
        except:
            # handle empty dataframe
            diff = list(set(cols) - set(df.columns.tolist()))
            df[diff] = ""
            df = df[cols]

        return df

    def calculate_no_overlap(self) -> None:
        # calculate expected sales
        ElementHelper.calculate_part(
            df=self.no_overlap,
            target="expected_sales",
            mode="half_end",
            first_col="delivery_date",
            second_col="next_delivery_date",
        )

        # add flag for trend detection
        self.no_overlap["half_start"] = 0
        self.no_overlap["half_end"] = 1

        # calculate est remaining stock
        self.no_overlap["est_remaining_stock"] = 0

        # get overlap
        self.no_overlap["overlap"] = 0

        # add adjustments
        ReturnRiskAdjustment.weekday_weekend_adjustment(df=self.no_overlap)
        adjustment_cols = [
            x for x in self.no_overlap.columns if x.endswith("_adjustment")
        ]
        self.no_overlap["total_adjustment"] = self.no_overlap[adjustment_cols].sum(
            axis=1
        )

        # calculate SO qty
        self.no_overlap["SO"] = self.no_overlap.apply(
            lambda x: CalculationHelper.single_final_so_qty(
                x.est_remaining_stock,
                x.overlap,
                x.expected_sales,
                so_param.buffer(x.return_risk * (1 + x.total_adjustment)),
            ),
            axis=1,
        )

        # valid remaining stock
        self.no_overlap["valid_remaining_stock"] = 0

        # buffer stock
        self.no_overlap["buffer_stock"] = self.no_overlap.apply(
            lambda x: (so_param.buffer(x.return_risk * (1 + x.total_adjustment)) - 1)
            * x.expected_sales,
            axis=1,
        )

    def calculate_single_overlap(self) -> None:
        # calculate expected sales
        single_overlap_half = self.single_overlap.loc[
            self.single_overlap["next_valid"]
            < self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)
        single_overlap_full = self.single_overlap.loc[
            self.single_overlap["next_valid"]
            >= self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)

        ElementHelper.calculate_part(
            df=single_overlap_half,
            target="expected_sales",
            mode="half_start",
            first_col="delivery_date",
            second_col="next_valid",
        )

        ElementHelper.calculate_part(
            df=single_overlap_full,
            target="expected_sales",
            mode="half_start_half_end",
            first_col="delivery_date",
            second_col="next_delivery_date",
        )

        # add flag for trend detection
        single_overlap_half["half_start"] = 1
        single_overlap_half["half_end"] = 0
        single_overlap_full["half_start"] = 1
        single_overlap_full["half_end"] = 1

        # calculate est remaining stock
        ElementHelper.calculate_part(
            df=single_overlap_half,
            target="sales_remaining_stock",
            mode="half_end",
            first_col="prev_delivery_date",
            second_col="delivery_date",
        )

        ElementHelper.calculate_part(
            df=single_overlap_full,
            target="sales_remaining_stock",
            mode="half_start_half_end",
            first_col="prev_delivery_date",
            second_col="delivery_date",
        )

        self.single_overlap = pd.concat(
            [single_overlap_half, single_overlap_full], ignore_index=True
        )

        del single_overlap_half, single_overlap_full

        prev_delivery_dates = (
            self.single_overlap["prev_delivery_date"]
            .apply(lambda x: x.strftime("%Y-%m-%d"))
            .unique()
            .tolist()
        )
        filtered_sales_order = self.sales_order[["id"] + prev_delivery_dates]

        rename_cols = {}
        for i, date in enumerate(prev_delivery_dates):
            rename_cols[date] = "SO_" + date
        filtered_sales_order = filtered_sales_order.rename(rename_cols, axis=1)

        self.single_overlap = pd.merge(
            self.single_overlap, filtered_sales_order, on=["id"], how="left"
        )

        del filtered_sales_order

        for i in range(len(prev_delivery_dates)):
            self.single_overlap.loc[
                self.single_overlap["prev_delivery_date"].apply(
                    lambda x: x.strftime("%Y-%m-%d")
                )
                == prev_delivery_dates[i],
                "SO_remaining_stock",
            ] = self.single_overlap["SO_" + prev_delivery_dates[i]]

        self.single_overlap["est_remaining_stock"] = (
            self.single_overlap["SO_remaining_stock"]
            - self.single_overlap["sales_remaining_stock"]
        )

        # put limit on remaining stock
        CalculationHelper.limit_remaining_stock(df=self.single_overlap)

        # get overlap
        single_overlap_half = self.single_overlap.loc[
            self.single_overlap["last_valid"]
            < self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)
        single_overlap_full = self.single_overlap.loc[
            self.single_overlap["last_valid"]
            == self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)

        ElementHelper.calculate_part(
            df=single_overlap_half,
            target="overlap",
            mode="half_start",
            first_col="delivery_date",
            second_col="last_valid",
        )

        single_overlap_full["overlap"] = single_overlap_full["expected_sales"]

        self.single_overlap = pd.concat(
            [single_overlap_half, single_overlap_full], ignore_index=True
        )

        del single_overlap_half, single_overlap_full

        # add adjustments
        ReturnRiskAdjustment.weekday_weekend_adjustment(df=self.single_overlap)
        adjustment_cols = [
            x for x in self.single_overlap.columns if x.endswith("_adjustment")
        ]
        self.single_overlap["total_adjustment"] = self.single_overlap[
            adjustment_cols
        ].sum(axis=1)

        # calculate SO qty
        self.single_overlap["SO"] = self.single_overlap.apply(
            lambda x: CalculationHelper.single_final_so_qty(
                x.est_remaining_stock,
                x.overlap,
                x.expected_sales,
                so_param.buffer(x.return_risk * (1 + x.total_adjustment)),
            ),
            axis=1,
        )

        # valid remaining stock
        self.single_overlap["valid_remaining_stock"] = self.single_overlap.apply(
            lambda x: x.est_remaining_stock
            if x.est_remaining_stock < x.overlap
            else x.overlap,
            axis=1,
        )

        # buffer stock
        self.single_overlap["buffer_stock"] = self.single_overlap.apply(
            lambda x: (so_param.buffer(x.return_risk * (1 + x.total_adjustment)) - 1)
            * x.expected_sales,
            axis=1,
        )

    def calculate_multiple_overlap(self) -> None:
        # calculate expected sales
        ElementHelper.calculate_part(
            df=self.multiple_overlap,
            target="expected_sales",
            mode="half_start_half_end",
            first_col="delivery_date",
            second_col="next_delivery_date",
        )

        # add flag for trend detection
        self.multiple_overlap["half_start"] = 1
        self.multiple_overlap["half_end"] = 1

        # calculate est remaining stock
        self.multiple_overlap["backdate"] = self.multiple_overlap.apply(
            lambda x: x.delivery_date - datetime.timedelta(x.shelf_life), axis=1
        )

        delivery_date_schedule, first_date = ScheduleHelper.generate_multiple_date_list(
            ScheduleHelper,
            df=self.multiple_overlap,
            first_col="backdate",
            second_col="delivery_date",
        )

        self.multiple_overlap["backdate"] = first_date

        sales_range = pd.date_range(
            start=datetime.datetime.strptime(min(first_date), "%Y-%m-%d"),
            end=self.train_end,
        )
        sales_range = [x.strftime("%Y-%m-%d") for x in sales_range]
        filtered_sales = self.sales[["id"] + sales_range]

        self.multiple_overlap = pd.merge(
            self.multiple_overlap, filtered_sales, on=["id"], how="left"
        )
        del filtered_sales

        ElementHelper.calculate_part(
            df=self.multiple_overlap,
            target="sales_remaining_stock",
            mode="half_start_half_end",
            first_col="backdate",
            second_col="delivery_date",
        )

        sales_order_range = pd.date_range(
            start=datetime.datetime.strptime(min(first_date), "%Y-%m-%d"),
            end=self.multiple_overlap["prev_delivery_date"].max(),
        )
        sales_order_range = [x.strftime("%Y-%m-%d") for x in sales_order_range]

        filtered_sales_order = self.sales_order[["id"] + sales_order_range]

        rename_cols = {}
        for i, date in enumerate(sales_order_range):
            rename_cols[date] = "SO_" + date
        filtered_sales_order = filtered_sales_order.rename(rename_cols, axis=1)

        self.multiple_overlap = pd.merge(
            self.multiple_overlap, filtered_sales_order, on=["id"], how="left"
        )
        del filtered_sales_order

        for entry in delivery_date_schedule:
            for i, date in enumerate(entry):
                entry[i] = "SO_" + date

        ElementHelper.calculate_part(
            df=self.multiple_overlap,
            target="SO_remaining_stock",
            mode="full",
            datelist_type="multiple",
            range_list=delivery_date_schedule,
        )

        self.multiple_overlap["est_remaining_stock"] = (
            self.multiple_overlap["SO_remaining_stock"]
            - self.multiple_overlap["sales_remaining_stock"]
        )

        # put limit on remaining stock
        CalculationHelper.limit_remaining_stock(df=self.multiple_overlap)

        # add adjustments
        ReturnRiskAdjustment.weekday_weekend_adjustment(df=self.multiple_overlap)
        adjustment_cols = [
            x for x in self.multiple_overlap.columns if x.endswith("_adjustment")
        ]
        self.multiple_overlap["total_adjustment"] = self.multiple_overlap[
            adjustment_cols
        ].sum(axis=1)

        # SO qty
        self.multiple_overlap["SO"] = self.multiple_overlap.apply(
            lambda x: CalculationHelper.multiple_final_so_qty(
                x.est_remaining_stock,
                x.expected_sales,
                so_param.buffer(x.return_risk * (1 + x.total_adjustment)),
            ),
            axis=1,
        )

        # valid remaining stock
        self.multiple_overlap["valid_remaining_stock"] = self.multiple_overlap[
            "est_remaining_stock"
        ]

        # buffer stock
        self.multiple_overlap["buffer_stock"] = self.multiple_overlap.apply(
            lambda x: (so_param.buffer(x.return_risk * (1 + x.total_adjustment)) - 1)
            * x.expected_sales,
            axis=1,
        )

    def calculate(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        # calculate initial SO quantity
        if len(self.no_overlap) != 0:
            self.calculate_no_overlap()
        if len(self.single_overlap) != 0:
            self.calculate_single_overlap()
        if len(self.multiple_overlap) != 0:
            self.calculate_multiple_overlap()

        # remap date to F1, F2, etc
        date_to_f = {}
        for i, date in enumerate(
            pd.date_range(start=self.forecast_start, end=self.forecast_end)
        ):
            date_to_f[date.strftime("%Y-%m-%d")] = "F" + str(i + 1)

        self.no_overlap = self.no_overlap.rename(date_to_f, axis=1)
        self.single_overlap = self.single_overlap.rename(date_to_f, axis=1)
        self.multiple_overlap = self.multiple_overlap.rename(date_to_f, axis=1)

        # get trend input
        no_overlap_t = self.no_overlap.copy()
        single_overlap_t = self.single_overlap.copy()
        multiple_overlap_t = self.multiple_overlap.copy()

        no_overlap_t = self.reindex_columns(
            df=no_overlap_t, cols=output_columns.TREND_INPUT
        )
        single_overlap_t = self.reindex_columns(
            df=single_overlap_t, cols=output_columns.TREND_INPUT
        )
        multiple_overlap_t = self.reindex_columns(
            df=multiple_overlap_t, cols=output_columns.TREND_INPUT
        )

        self.trend_input = pd.concat(
            [no_overlap_t, single_overlap_t, multiple_overlap_t], ignore_index=True
        )
        del no_overlap_t, single_overlap_t, multiple_overlap_t

        self.trend_input = self.trend_input.rename(
            {"delivery_date": "start_at", "next_delivery_date": "end_at"}, axis=1
        )

        self.trend_input["start_at"] = self.trend_input["start_at"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )
        self.trend_input["end_at"] = self.trend_input["end_at"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )

        self.trend_input = self.trend_input.sort_values(
            by=["id", "outlet_group"], ignore_index=True
        ).drop_duplicates(subset=["id", "start_at"], ignore_index=True)
        self.trend_input = self.trend_input.drop(["outlet_group"], axis=1)

        # get SO quantity
        if "valid_remaining_stock" in self.no_overlap.columns:
            self.no_overlap = self.no_overlap.drop(["est_remaining_stock"], axis=1)
            self.no_overlap = self.no_overlap.rename(
                {"valid_remaining_stock": "est_remaining_stock"}, axis=1
            )
        if "valid_remaining_stock" in self.single_overlap.columns:
            self.single_overlap = self.single_overlap.drop(
                ["est_remaining_stock"], axis=1
            )
            self.single_overlap = self.single_overlap.rename(
                {"valid_remaining_stock": "est_remaining_stock"}, axis=1
            )
        if "valid_remaining_stock" in self.multiple_overlap.columns:
            self.multiple_overlap = self.multiple_overlap.drop(
                ["est_remaining_stock"], axis=1
            )
            self.multiple_overlap = self.multiple_overlap.rename(
                {"valid_remaining_stock": "est_remaining_stock"}, axis=1
            )

        # placeholder for sales ratio
        self.no_overlap["sales_ratio"] = 0
        self.single_overlap["sales_ratio"] = 0
        self.multiple_overlap["sales_ratio"] = 0

        self.no_overlap = self.reindex_columns(
            df=self.no_overlap, cols=output_columns.FORECAST
        )
        self.single_overlap = self.reindex_columns(
            df=self.single_overlap, cols=output_columns.FORECAST
        )
        self.multiple_overlap = self.reindex_columns(
            df=self.multiple_overlap, cols=output_columns.FORECAST
        )
        self.multiple_overlap["overlap"] = self.multiple_overlap["overlap"].replace(
            {"": np.nan}
        )

        self.final_forecast = pd.concat(
            [self.no_overlap, self.single_overlap, self.multiple_overlap],
            ignore_index=True,
        )
        del self.no_overlap, self.single_overlap, self.multiple_overlap

        self.final_forecast = self.final_forecast.sort_values(
            by=["id", "outlet_group"], ignore_index=True
        ).drop_duplicates(subset=["id", "delivery_date"], ignore_index=True)

        self.final_forecast["SO"] = (
            self.final_forecast["SO"].apply(lambda x: np.round(x)).astype(int)
        )

        # adjust quantity
        self.final_forecast["SO"] = self.final_forecast["SO"].apply(
            lambda x: 0 if x < 0 else x
        )
        try:
            self.final_forecast["delivery_date"] = self.final_forecast[
                "delivery_date"
            ].apply(lambda x: x.strftime("%Y-%m-%d"))
        except:
            pass
        delivery_dates = self.final_forecast["delivery_date"].unique().tolist()
        for ddate in delivery_dates:
            self.final_forecast.loc[
                self.final_forecast["delivery_date"] == ddate, "SO"
            ] = self.final_forecast.loc[
                self.final_forecast["delivery_date"] == ddate
            ].apply(
                lambda x: 0 if x.id.split("_")[2] in self.new_products[ddate] else x.SO,
                axis=1,
            )
        self.final_forecast = self.QuantityAdjustment.min_qty_adjustment(
            df=self.final_forecast
        )
        self.final_forecast = self.QuantityAdjustment.min_new_adjustment(
            df=self.final_forecast
        )

        # enforce output columns, cast dtype, and sort values
        self.final_forecast = self.final_forecast[output_columns.FORECAST]
        self.final_forecast["overlap"] = self.final_forecast["overlap"].fillna(0)

        # calculate sales ratio
        self.final_forecast["sales_ratio"] = self.final_forecast.apply(
            lambda x: CalculationHelper.calculate_sales_ratio(
                x.expected_sales, x.SO, x.est_remaining_stock
            ),
            axis=1,
        )

        return self.final_forecast, self.trend_input


class SOCalculationV1:
    """Calculate SO quantity using CL_v1. Contains:
    - *`reindex_columns`: Reindex dataframe columns.
    - *`calculate_no_overlap`
    - *`calculate_single_overlap`
    - *`calculate`: Calculate SO quantity using CL_v1 for all IDs. \n
    (* = need to instantiate class)"""

    def __init__(
        self,
        forecast: pd.DataFrame,
        sales_order: pd.DataFrame,
        new_products: Dict[str, str],
        windows: List[datetime.datetime],
    ) -> None:

        self.forecast = forecast
        self.sales_order = sales_order
        self.new_products = new_products
        _, _, self.forecast_start, self.forecast_end = windows

        self.QuantityAdjustment = QuantityAdjustment(new_products=self.new_products)

        # initialize SO column and overwrite shelf life
        self.forecast["SO"] = 0
        self.forecast["shelf_life"] = so_param.DEFAULT_SHELF_LIFE

        # match ID between forecast and historical sales order
        self.forecast = utils.filter_id(
            df=self.forecast,
            col="id",
            selected=self.sales_order["id"].unique().tolist(),
        )
        self.sales_order = utils.filter_id(
            df=self.sales_order,
            col="id",
            selected=self.forecast["id"].unique().tolist(),
        )

        # shelf life validation
        (
            self.no_overlap,
            self.single_overlap,
            _,
        ) = CalculationHelper.shelf_life_validation(df=self.forecast)
        del self.forecast

    def reindex_columns(self, df: pd.DataFrame, cols: List[Any]) -> None:
        """Reindex dataframe columns."""
        try:
            df = df[cols]
        except:
            # handle empty dataframe
            diff = list(set(cols) - set(df.columns.tolist()))
            df[diff] = ""
            df = df[cols]

        return df

    def calculate_no_overlap(self) -> None:
        """Calculate elements for no overlap."""
        # calculate remaining stock
        self.no_overlap["est_remaining_stock"] = 0

        # calculate overlap
        self.no_overlap["overlap"] = 0

        # calculate expected sales
        ElementHelper.calculate_part(
            df=self.no_overlap,
            target="expected_sales",
            mode="half_end",
            first_col="delivery_date",
            second_col="next_delivery_date",
        )

        self.no_overlap["half_start"] = 0
        self.no_overlap["half_end"] = 1

        # calculate SO and buffer stock
        self.no_overlap["SO"] = self.no_overlap.apply(
            lambda x: CalculationHelper.single_final_so_qty(
                x.est_remaining_stock,
                x.overlap,
                x.expected_sales,
                so_param.buffer(x.return_risk),
            ),
            axis=1,
        )
        self.no_overlap["SO"] = (
            self.no_overlap["SO"].apply(lambda x: np.round(x, decimals=0)).astype(int)
        )

        self.no_overlap["buffer_stock"] = self.no_overlap.apply(
            lambda x: (so_param.buffer(x.return_risk) - 1) * x.expected_sales, axis=1
        )

    def calculate_single_overlap(self) -> None:
        """Calculate elements for single overlap."""
        # get SO qty on previous delivery
        min_date = min(self.single_overlap["prev_delivery_date"].unique().tolist())
        max_date = max(self.single_overlap["prev_delivery_date"].unique().tolist())
        so_range = [
            x.strftime("%Y-%m-%d") for x in pd.date_range(start=min_date, end=max_date)
        ]
        self.sales_order = self.sales_order[["id"] + so_range]

        for col in so_range:
            self.sales_order = self.sales_order.rename({col: f"SO_{col}"}, axis=1)

        self.single_overlap = pd.merge(
            self.single_overlap, self.sales_order, on=["id"], how="left"
        )
        del self.sales_order

        self.single_overlap["SO_remaining_stock"] = self.single_overlap.apply(
            lambda x: x[f"SO_{x.prev_delivery_date.strftime('%Y-%m-%d')}"], axis=1
        )

        # separate DataFrame based on overlap
        single_overlap_half = self.single_overlap.loc[
            self.single_overlap["next_valid"]
            < self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)
        single_overlap_full = self.single_overlap.loc[
            self.single_overlap["next_valid"]
            >= self.single_overlap["next_delivery_date"]
        ].reset_index(drop=True)
        del self.single_overlap

        self.single_overlap = pd.DataFrame()
        if len(single_overlap_half) != 0:
            # get sales remaining stock
            ElementHelper.calculate_part(
                df=single_overlap_half,
                target="sales_remaining_stock",
                mode="half_end",
                first_col="prev_delivery_date",
                second_col="delivery_date",
            )

            # calculate remaining stock
            single_overlap_half["est_remaining_stock"] = (
                single_overlap_half["SO_remaining_stock"]
                - single_overlap_half["sales_remaining_stock"]
            )

            # calculate overlap
            ElementHelper.calculate_part(
                df=single_overlap_half,
                target="overlap",
                mode="half_start",
                first_col="delivery_date",
                second_col="last_valid",
            )

            # calculate expected sales
            ElementHelper.calculate_part(
                df=single_overlap_half,
                target="expected_sales",
                mode="half_start",
                first_col="delivery_date",
                second_col="next_valid",
            )
            single_overlap_half["half_start"] = 1
            single_overlap_half["half_end"] = 0

            # concatenate
            self.single_overlap = pd.concat(
                [self.single_overlap, single_overlap_half], ignore_index=True
            )

        if len(single_overlap_full) != 0:
            # get sales remaining stock
            ElementHelper.calculate_part(
                df=single_overlap_full,
                target="sales_remaining_stock",
                mode="half_start_half_end",
                first_col="prev_delivery_date",
                second_col="delivery_date",
            )

            # calculate remaining stock
            single_overlap_full["est_remaining_stock"] = (
                single_overlap_full["SO_remaining_stock"]
                - single_overlap_full["sales_remaining_stock"]
            )

            # calculate overlap
            ElementHelper.calculate_part(
                df=single_overlap_full,
                target="overlap",
                mode="half_start",
                first_col="delivery_date",
                second_col="last_valid",
            )

            # calculate expected sales
            ElementHelper.calculate_part(
                df=single_overlap_full,
                target="expected_sales",
                mode="half_start_half_end",
                first_col="delivery_date",
                second_col="next_delivery_date",
            )

            single_overlap_full["half_start"] = 1
            single_overlap_full["half_end"] = 1

            # concatenate
            self.single_overlap = pd.concat(
                [self.single_overlap, single_overlap_full], ignore_index=True
            )

        del single_overlap_half, single_overlap_full

        # calculate SO and buffer stock
        self.single_overlap["SO"] = self.single_overlap.apply(
            lambda x: CalculationHelper.single_final_so_qty(
                x.est_remaining_stock,
                x.overlap,
                x.expected_sales,
                so_param.buffer(x.return_risk),
            ),
            axis=1,
        )
        self.single_overlap["SO"] = (
            self.single_overlap["SO"]
            .apply(lambda x: np.round(x, decimals=0))
            .astype(int)
        )

        self.single_overlap["buffer_stock"] = self.single_overlap.apply(
            lambda x: (so_param.buffer(x.return_risk) - 1) * x.expected_sales, axis=1
        )

    def calculate(self) -> None:
        # calculate elements
        if len(self.no_overlap) != 0:
            self.calculate_no_overlap()
        if len(self.single_overlap) != 0:
            self.calculate_single_overlap()

        # placeholder for sales ratio
        self.no_overlap["sales_ratio"] = 0
        self.single_overlap["sales_ratio"] = 0

        # remap date to F1, F2, etc
        date_to_f = {}
        for i, date in enumerate(
            pd.date_range(start=self.forecast_start, end=self.forecast_end)
        ):
            date_to_f[date.strftime("%Y-%m-%d")] = "F" + str(i + 1)

        self.no_overlap = self.no_overlap.rename(date_to_f, axis=1)
        self.single_overlap = self.single_overlap.rename(date_to_f, axis=1)

        # * get trend input
        no_overlap_t = self.no_overlap.copy()
        single_overlap_t = self.single_overlap.copy()

        no_overlap_t = self.reindex_columns(
            df=no_overlap_t, cols=output_columns.TREND_INPUT
        )
        single_overlap_t = self.reindex_columns(
            df=single_overlap_t, cols=output_columns.TREND_INPUT
        )

        self.trend_input = pd.concat(
            [no_overlap_t, single_overlap_t], ignore_index=True
        )
        del no_overlap_t, single_overlap_t

        self.trend_input = self.trend_input.rename(
            {"delivery_date": "start_at", "next_delivery_date": "end_at"}, axis=1
        )

        self.trend_input["start_at"] = self.trend_input["start_at"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )
        self.trend_input["end_at"] = self.trend_input["end_at"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )

        self.trend_input = (
            self.trend_input.sort_values(by=["id", "outlet_group"], ignore_index=True)
            .drop_duplicates(subset=["id", "start_at"], ignore_index=True)
            .drop(["outlet_group"], axis=1)
        )

        # * get SO quantity
        # reindex columns
        self.no_overlap = self.reindex_columns(
            df=self.no_overlap, cols=output_columns.FORECAST + ["return_risk"]
        )
        self.single_overlap = self.reindex_columns(
            df=self.single_overlap, cols=output_columns.FORECAST + ["return_risk"]
        )

        # concatenate
        self.final_forecast = pd.concat(
            [self.no_overlap, self.single_overlap], ignore_index=True
        )
        del self.no_overlap, self.single_overlap

        # drop duplicate rows
        self.final_forecast = self.final_forecast.sort_values(
            by=["id", "outlet_group"], ignore_index=True
        ).drop_duplicates(subset=["id", "delivery_date"], ignore_index=True)

        # adjust quantity
        self.final_forecast["SO"] = self.final_forecast["SO"].apply(
            lambda x: 0 if x < 0 else x
        )
        try:
            self.final_forecast["delivery_date"] = self.final_forecast[
                "delivery_date"
            ].apply(lambda x: x.strftime("%Y-%m-%d"))
        except:
            pass
        delivery_dates = self.final_forecast["delivery_date"].unique().tolist()
        for ddate in delivery_dates:
            self.final_forecast.loc[
                self.final_forecast["delivery_date"] == ddate, "SO"
            ] = self.final_forecast.loc[
                self.final_forecast["delivery_date"] == ddate
            ].apply(
                lambda x: 0 if x.id.split("_")[2] in self.new_products[ddate] else x.SO,
                axis=1,
            )
        self.final_forecast = self.QuantityAdjustment.min_qty_adjustment(
            df=self.final_forecast
        )
        self.final_forecast = self.QuantityAdjustment.min_new_adjustment(
            df=self.final_forecast
        )

        # calculate sales ratio
        self.final_forecast["sales_ratio"] = self.final_forecast.apply(
            lambda x: CalculationHelper.calculate_sales_ratio(
                x.expected_sales, x.SO, x.est_remaining_stock
            ),
            axis=1,
        )

        # enforce output columns
        self.final_forecast = self.final_forecast[output_columns.FORECAST]

        return self.final_forecast, self.trend_input
