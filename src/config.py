import logging
import os

import numpy as np
from pandas.core.common import SettingWithCopyWarning


class GeneralConfig:
    def __init__(self) -> None:
        # Logging and Warning configuration
        logging.basicConfig(
            format="%(asctime)s %(levelname)s: %(message)s",
            datefmt="%Y/%m/%d %H:%M:%S",
            level=logging.INFO,
        )


class SOParam:
    def __init__(self) -> None:
        self.DEFAULT_RETURN_RISK = 0.26
        self.MIN_DELIVERY_QTY = 8
        self.NEW_QTY_GROUP_C = 2
        self.NEW_QTY_GROUP_E = 3
        self.NEW_QTY_OTHER = 5

        self.PCT_MIN_GROUP_E = -0.15
        self.PCT_MAX_GROUP_E = 0.15

        self.PCT_MIN_GROUP_C_WEND = -0.075
        self.PCT_MAX_GROUP_C_WEND = 0.15
        self.PCT_MIN_GROUP_C_WDAY = -0.15
        self.PCT_MAX_GROUP_C_WDAY = 0.075

        self.PCT_PAYDAY = 0.08
        self.PCT_NONPAYDAY = -0.12

        self.DEFAULT_SHELF_LIFE = 4

    def buffer(self, value) -> float:
        self.BUFFER = 1 / (1 - value)
        return self.BUFFER


class ScheduleParam:
    def __init__(self) -> None:
        self.DAYSOFWEEK = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")

        self.DELIVERY_SCHEDULE = {
            "C1": ["Mon", "Wed", "Fri"],
            "C2": ["Tue", "Thu", "Sat"],
            "E1": ["Mon", "Fri"],
            "E2": ["Tue", "Sat"],
            "E3": ["Wed", "Sun"],
            "E4": ["Thu", "Sun"],
        }
        self.DELIVERY_TO_FORECAST = {
            "Mon": "Thu",
            "Tue": "Fri",
            "Wed": "Fri",
            "Thu": "Mon",
            "Fri": "Tue",
            "Sat": "Wed",
            "Sun": "Thu",
        }

        self.WEEKDAY_DELIVERY_C = ["Mon", "Tue", "Wed", "Thu"]
        self.WEEKEND_DELIVERY_C = ["Fri", "Sat"]

        self.WEEKDAY_DELIVERY_E = ["Sun", "Mon", "Tue"]
        self.WEEKEND_DELIVERY_E = ["Wed", "Thu", "Fri", "Sat"]

        self.PAYDAY_PERIOD = [x for x in range(1, 11)] + [x for x in range(23, 32)]


class FeatureParam:
    def __init__(self) -> None:
        self.VALUE = [7, 13, 20]


class OutputColumns:
    def __init__(self) -> None:
        self.TREND_INPUT = [
            "id",
            "expected_sales",
            "delivery_date",
            "next_delivery_date",
            "half_start",
            "half_end",
            "outlet_group",
        ]

        self.FORECAST = [
            "id",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "F6",
            "F7",
            "F8",
            "F9",
            "F10",
            "F11",
            "F12",
            "est_remaining_stock",
            "expected_sales",
            "buffer_stock",
            "SO",
            "outlet_group",
            "delivery_date",
            "sales_ratio",
            "forecast_type",
            "overlap",
        ]


class DownloadLocation:
    def __init__(self, download_path: str) -> None:
        self.SHELF_LIFE = os.path.join(download_path, "shelf_life.csv")
        self.DAILY_TARGET = os.path.join(download_path, "param_target_sample.csv")
