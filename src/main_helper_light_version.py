import numpy as np
import pandas as pd
import polars as pl
import datetime

import logging
import sys
from downcast import reduce
from src import utils_light_version as utils
from src.config import OutputColumns, SOParam
from src.so_calculation.calculate_so import (
    ScheduleHelper,
    )
from src.feature_engineering.feature_bottom_up import (
    label_encoding,
    mean_encoding,
    sales_feature_engineering,
)
from src.training import train_bottom_up

from typing import Dict, List

so_param = SOParam()
output_columns = OutputColumns()


def split_list(
    alist: List,
    wanted_parts: int,
):
    length = len(alist)
    return [
        alist[i * length // wanted_parts : (i + 1) * length // wanted_parts]
        for i in range(wanted_parts)
    ]


def prepare_for_calculation(*args) -> pd.DataFrame:
    """Merge DataFrame with supporting information for SO calculation."""
    (
        df,
        windows,
        category_map,
        today_day,
        outlet_group,
        shelf_life,
        outlet_rank,
        return_risk,
        store_type,
        today_date,
    ) = args
    _, _, forecast_start, forecast_end = windows

    # initialize columns
    df["dc_code"] = df["id"].apply(lambda x: x.split("_")[0])
    df["outlet_code"] = df["id"].apply(lambda x: x.split("_")[1])
    df["product_plu"] = df["id"].apply(lambda x: x.split("_")[2])
    df["SO"] = 0

    # add outlet group information
    df = pd.merge(df, outlet_group, on=["outlet_code"], how="left")

    # add category information
    df["category_id"] = df["product_plu"].astype(int).map(category_map)

    # add shelf life information
    df["shelf_life"] = (
        df["product_plu"]
        .astype(int)
        .map(dict(zip(shelf_life.product_plu, shelf_life.shelf_life)))
    )

    # add return risk information
    df = pd.merge(df, outlet_rank, on=["outlet_code"], how="left")
    return_risk["product_plu"] = return_risk["product_plu"].astype(str)
    df = pd.merge(
        df, return_risk, on=["dc_code", "outlet_rank", "product_plu"], how="left"
    )
    df["return_risk"].fillna(so_param.DEFAULT_RETURN_RISK, inplace=True)
    df["return_risk"].replace({1: 0.99}, inplace=True)

    # add outlet type information
    df = pd.merge(df, store_type, on=["outlet_code", "category_id"], how="left")

    # add offset and sequential deliveries
    ScheduleHelper.add_offset(df=df, today_day=today_day)
    ScheduleHelper.generate_sequential_delivery(df=df, today_date=today_date)

    # convert F1, F2, etc to corresponding date
    f_to_date = {}
    for i, date in enumerate(pd.date_range(start=forecast_start, end=forecast_end)):
        f_to_date["F" + str(i + 1)] = datetime.datetime.strftime(date, "%Y-%m-%d")
    df.rename(f_to_date, axis=1, inplace=True)

    return df


def get_new_products(
    release_info: pd.DataFrame, delivery_dates: List[datetime.datetime]
) -> Dict[str, str]:
    """Return list of products still in launching period."""
    new_plus = {}
    for delivery_date in delivery_dates:
        new_plus[delivery_date.strftime("%Y-%m-%d")] = (
            release_info.loc[
                (
                    release_info["release_date"]
                    > (delivery_date - datetime.timedelta(days=28)).strftime("%Y-%m-%d")
                )
                & (release_info["release_date"] < delivery_date.strftime("%Y-%m-%d"))
            ]["product_plu"]
            .unique()
            .tolist()
        )
        new_plus[delivery_date.strftime("%Y-%m-%d")] = [
            str(x) for x in new_plus[delivery_date.strftime("%Y-%m-%d")]
        ]

    return new_plus


# check if DC exists in delivery_group.csv
def filter_delivery(
    target,
    master_delivery_group,
    CLOSED_STATUS,
    today_date,
    sales_init=None,
    forecast=None,
    store_type=None,
):
    if (target not in master_delivery_group["dc_code"].unique().tolist()) and (
        target != "all"
    ):
        logging.info(f"No schedule found for {target}, skipping.")
        sys.exit(0)

    # Filter out closed outlets
    master_delivery_group = master_delivery_group.loc[
        ~master_delivery_group["loading_number"].isin(CLOSED_STATUS)
    ].reset_index(drop=True)

    if target != "all":
        logging.info("target is not all")
        master_delivery_group = master_delivery_group.loc[
            (master_delivery_group["dc_code"] == target)
            & (
                master_delivery_group["outlet_group"].isin(
                    forecast["outlet_group"].unique().tolist()
                )
            )
        ]
    else:  # Filter out DCs not in sales data
        logging.info("filtering out DCs")
        master_delivery_group = utils.filter_id(
            df=master_delivery_group,
            col="dc_code",
            selected=sales_init["dc_code"].unique().tolist(),
        )

    delivery_group = master_delivery_group.loc[
        (
            master_delivery_group["opening_date"]
            < (today_date - datetime.timedelta(days=28)).strftime("%Y-%m-%d")
        )
    ].reset_index(drop=True)

    semi_go_outlet_info = delivery_group.loc[
        (
            delivery_group["opening_date"]
            < (today_date - datetime.timedelta(days=28)).strftime("%Y-%m-%d")
        )
        & (
            delivery_group["opening_date"]
            >= (today_date - datetime.timedelta(days=84)).strftime("%Y-%m-%d")
        )
    ]
    semi_go_outlets = semi_go_outlet_info["outlet_code"].unique().tolist()
    delivery_group = delivery_group.drop(["loading_number", "opening_date"], axis=1)

    if target != "all":
        go_outlets = (
            master_delivery_group.loc[
                (
                    master_delivery_group["opening_date"]
                    >= (today_date - datetime.timedelta(days=28)).strftime("%Y-%m-%d")
                )
                & (
                    master_delivery_group["opening_date"]
                    < today_date.strftime("%Y-%m-%d")
                )
            ]["outlet_code"]
            .unique()
            .tolist()
        )

        # Filter to target DC
        target_list = []
        target_list.append(target)

        forecast["dc_code"] = forecast["id"].apply(lambda x: x.split("_")[0])
        forecast["outlet_code"] = forecast["id"].apply(lambda x: x.split("_")[1])
        forecast["product_plu"] = forecast["id"].apply(lambda x: x.split("_")[1])
        forecast = utils.filter_id(df=forecast, col="dc_code", selected=target_list)
        forecast.drop(["dc_code"], axis=1, inplace=True)

        store_type = utils.filter_id(
            df=store_type,
            col="outlet_code",
            selected=forecast["outlet_code"].unique().tolist(),
        )

        delivery_group = utils.filter_id(
            df=delivery_group,
            col="outlet_code",
            selected=forecast["outlet_code"].unique().tolist(),
        )
        return (
            master_delivery_group,
            delivery_group,
            store_type,
            forecast,
            go_outlets,
            semi_go_outlets,
        )
    else:
        return master_delivery_group, delivery_group



def bottom_up_process(
        preprocess, today_day, today_date, submission_range
):

    # Feature engineering with Training and prediction
    filtered_bu = preprocess.sales.copy()
    
    # get day number of earliest delivery date
    if today_day == "Fri":
        min_delivery_d = preprocess.date_map[today_date.strftime("%Y-%m-%d")] + 4
    else:
        min_delivery_d = preprocess.date_map[today_date.strftime("%Y-%m-%d")] + 3

    outlet_train, conv_id, df_label = label_encoding(filtered_bu)
    df_label = mean_encoding(df_label)

    # Set split training size
    SPLIT_TRAINING = 10
    list_of_list = split_list(outlet_train, wanted_parts=SPLIT_TRAINING)
    submission_data_bu = []

    for i, loop_group in enumerate(list_of_list):
        feature_engineered_bu = sales_feature_engineering(
            df=df_label.loc[df_label["outlet_code"].isin(loop_group)],
            min_delivery_d=min_delivery_d,
        )

        feature_engineered_bu = feature_engineered_bu.sort([pl.col("id"), pl.col("d")])
        feature_engineered_bu = feature_engineered_bu.to_pandas()

        submission_bu = train_bottom_up.train_and_predict(
            df=feature_engineered_bu,
            conv_id=conv_id,
            n_days=len(submission_range),
        )
        submission_data_bu.append(submission_bu)

        logging.info(
            f"======= Processing BU group-{i} with {len(loop_group)} Outlet ========="
        )

    return submission_data_bu



