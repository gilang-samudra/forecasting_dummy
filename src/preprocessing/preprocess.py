import datetime
import logging
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
from downcast import reduce


class PreprocessHelper:
    """Helper functions for preprocessing. Contains:
    - *`create_date_map`: Create mapping from date to day number.
    - *`separate_columns`: Separate id columns and window columns.
    - *`filter_date`: Filter data based on training window and forecast window.
    - *`convert_to_day_number`: Convert date to day number. \n
    (* = need to instantiate class)"""

    def __init__(
        self, sales: pd.DataFrame, promo: pd.DataFrame, windows: List[datetime.datetime]
    ) -> None:
        self.sales = sales
        self.promo = promo
        self.windows = windows
        (
            self.train_start,
            self.train_end,
            self.forecast_start,
            self.forecast_end,
        ) = self.windows

    def create_date_map(self) -> Dict[str, int]:
        """Create mapping from date to day number."""

        # create date_map
        date_range = pd.date_range(
            start=self.train_start, end=self.forecast_end
        ).tolist()
        self.date_map = {}

        for i, date in enumerate(date_range):
            self.date_map[datetime.datetime.strftime(date, "%Y-%m-%d")] = i + 1

        return self.date_map

    def separate_columns(self) -> None:
        """Separate id columns and window columns."""

        self.date_map = self.create_date_map()

        # select training window
        sales_cols = self.sales.columns.tolist()
        for i, col in enumerate(sales_cols):
            if col == self.train_start.strftime("%Y-%m-%d"):
                start = i
            if col == self.train_end.strftime("%Y-%m-%d"):
                end = i
            try:  # to filter out all columns that contain date information
                datetime.datetime.strptime(col, "%Y-%m-%d")
            except:  # in the end of iteration, idx value is the index of the last string column
                idx = i

        self.id_cols = sales_cols[: idx + 1]

        try:
            self.window_cols = sales_cols[start : end + 1]
        except UnboundLocalError:
            logging.exception("Date mismatch in promotion or sales data!")

    def filter_date(
        self,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Filter data based on training window and forecast window."""

        self.separate_columns()
        training_cols = self.id_cols + self.window_cols
        self.sales = self.sales[training_cols]

        # select submission window and add 0
        submission_range = [
            d.strftime("%Y-%m-%d")
            for d in pd.date_range(
                start=self.forecast_start,
                end=self.forecast_end,
            )
        ]
        for d in submission_range:
            self.sales[d] = 0

        # select promotional window
        self.promo = self.promo[
            (self.promo["date"] >= self.train_start.strftime("%Y-%m-%d"))
            & (self.promo["date"] <= self.forecast_end.strftime("%Y-%m-%d"))
        ].reset_index(drop=True)

    def convert_to_day_number(self, df: pd.DataFrame) -> pd.DataFrame:
        """Convert date to day number."""

        self.date_map = self.create_date_map()

        df["date"] = df["date"].astype(str)
        df["d"] = df["date"].map(self.date_map).astype(np.int16)
        df.drop(["date"], axis=1, inplace=True)

        return df


class PromotionGenerator:
    """Generate promotion and 28 days after promotion period. Contains:
    - *`get_promo_period`: Get ID and d of promo period.
    - *`get_after_promo_period`: Get ID and d of after promo period.
    - *`generate`: Generate DataFrame for promo period and after promo period. \n
    (* = need to instantiate class)"""

    def __init__(self, df: pd.DataFrame, date_map: Dict[int, str]) -> None:
        self.df = df
        self.promo_cols = [x for x in self.df.columns if x.startswith("promo_")]

        self.date_map = date_map

    def get_promo_period(self) -> None:
        """Get ID and d of promo period."""
        self.during_promo = self.df[["id", "d"] + self.promo_cols].copy()

        # locate promotion period for every promotion type
        temp = self.during_promo.copy()
        self.during_promo = pd.DataFrame()
        for promo in self.promo_cols:
            self.during_promo = pd.concat(
                [self.during_promo, temp.loc[temp[promo] == 1]], ignore_index=True
            )

        # add flag to indicate promotion period
        self.during_promo[self.promo_cols] = self.during_promo[self.promo_cols].fillna(
            0
        )
        self.during_promo["flag_promo"] = self.during_promo[self.promo_cols].sum(axis=1)
        self.during_promo["flag_promo"] = self.during_promo["flag_promo"].apply(
            lambda x: 1 if x >= 1 else 0
        )
        self.during_promo.drop_duplicates(ignore_index=True, inplace=True)

        self.during_promo = self.during_promo[["id", "d", "flag_promo"]]

        del temp

    def get_after_promo_period(self) -> None:
        """Get ID and d of after promo period."""

        # get id and d for first day after promotion period
        after_promo_list = []
        for promo in self.promo_cols:
            for y in [
                list(x)
                for x in zip(
                    self.df.loc[
                        (self.df[promo].astype("int").diff() == -1)
                        & (self.df["id"].shift(1) == self.df["id"])
                    ].id,
                    self.df.loc[
                        (self.df[promo].astype("int").diff() == -1)
                        & (self.df["id"].shift(1) == self.df["id"])
                    ].d,
                )
            ]:
                after_promo_list.append(y)

        # construct dictionary for DataFrame for 28 days after promotion period
        if len(after_promo_list) > 0:
            y = {}
            for i in range(len(after_promo_list) * 28):
                z = {}
                z["id"] = after_promo_list[i // 28][0]
                z["d"] = after_promo_list[i // 28][1] + i % 28
                y[i] = z

            # create Dataframe and add flag to indicate after promotion period
            self.after_promo = pd.DataFrame.from_dict(data=y, orient="index")
            self.after_promo["flag_after_promo"] = 1

            self.after_promo = self.after_promo[["id", "d", "flag_after_promo"]]

        elif len(after_promo_list) == 0:
            self.after_promo = pd.DataFrame(columns=["id", "d", "flag_after_promo"])

    def generate(self) -> Tuple[pd.DataFrame, pd.DataFrame]:
        self.get_promo_period()
        self.get_after_promo_period()

        self.during_promo["delivery_date"] = self.during_promo["d"].map(self.date_map)
        self.after_promo["delivery_date"] = self.after_promo["d"].map(self.date_map)

        self.during_promo.dropna(inplace=True)
        self.after_promo.dropna(inplace=True)

        self.during_promo.drop(["d"], axis=1, inplace=True)
        self.after_promo.drop(["d"], axis=1, inplace=True)

        self.during_promo["delivery_date"] = self.during_promo["delivery_date"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")
        )
        self.after_promo["delivery_date"] = self.after_promo["delivery_date"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")
        )

        return (self.during_promo, self.after_promo)


class Preprocess(PreprocessHelper):
    """Prepare data for feature engineering. Contains:
    - *`generate_time_features`: Generate time-based features from date.
    - *`generate_promo_features`: Generate promotion features.
    - *`compute`: Generate preprocessed data. \n
    (* = need to instantiate class)"""

    def __init__(
        self, sales: pd.DataFrame, promo: pd.DataFrame, windows: List[datetime.datetime]
    ) -> None:
        super().__init__(sales, promo, windows)

    def generate_time_features(self) -> pd.DataFrame:
        """Generate time-based features from date."""

        self.time_features = self.sales["date"].copy().to_frame()
        self.time_features = self.time_features.drop_duplicates(ignore_index=True)

        self.time_features["date"] = self.time_features["date"].apply(
            lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")
        )

        self.time_features["day"] = self.time_features["date"].dt.day
        self.time_features["week"] = (
            self.time_features["date"].dt.isocalendar().week.astype(np.int8)
        )
        self.time_features["month"] = self.time_features["date"].dt.month
        self.time_features["year"] = self.time_features["date"].dt.year
        self.time_features["year"] = (
            self.time_features["year"] - self.time_features["year"].min()
        ).astype(np.int8)
        self.time_features["dayofweek"] = self.time_features["date"].dt.dayofweek
        self.time_features["dayofyear"] = self.time_features[
            "date"
        ].dt.dayofyear.astype(np.int16)
        self.time_features["weekofmonth"] = (
            self.time_features["day"].apply(lambda x: np.ceil(x / 7)).astype(np.int8)
        )
        self.time_features["quarter"] = self.time_features["date"].dt.quarter
        self.time_features["is_weekend"] = self.time_features["dayofweek"].apply(
            lambda x: 1 if x >= 5 else 0
        )
        self.time_features["is_month_start"] = self.time_features[
            "date"
        ].dt.is_month_start.astype(int)
        self.time_features["is_month_end"] = self.time_features[
            "date"
        ].dt.is_month_end.astype(int)

        self.time_features["date"] = self.time_features["date"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        )

        for event_type in [x for x in self.promo.columns if x.startswith("event_type")]:
            self.time_features[event_type] = (
                self.promo[event_type].astype("category").cat.codes
            )

    def generate_promo_features(self) -> None:
        """Generate promotion features."""

        promo_types = list(
            set(
                [
                    x.split("_")[2]
                    for x in self.promo.columns
                    if not (x.startswith("event_type") or x == "date")
                ]
            )
        )

        df_list = []
        cols = self.promo.columns.tolist()[1:]

        for promo_type in promo_types:
            cols = [x for x in self.promo.columns if x.endswith(promo_type)]

            for col in cols:
                temp = self.promo[["date"] + [col]].copy()

                temp.loc[temp[col] != 0, "dc_code"] = col.split("_")[0]
                temp.loc[temp[col] != 0, "product_plu"] = col.split("_")[1]
                temp.loc[
                    temp[col] != 0, "promo_" + promo_type.lower().replace(" ", "_")
                ] = 1

                temp = temp.drop([col], axis=1)
                temp = temp.dropna()

                df_list.append(temp)

            self.promo_features = pd.concat(df_list, ignore_index=True)
            self.promo_features.fillna(0, inplace=True)

    def compute(self) -> pd.DataFrame:
        # downcasting
        self.sales = reduce(self.sales)
        self.promo = reduce(self.promo)

        # filter date
        self.filter_date()

        # drop NaN id columns and replace NaN sales with 0
        self.sales.dropna(axis=0, subset=self.id_cols, inplace=True)
        self.sales.replace(np.nan, 0, inplace=True)

        # melt data
        self.sales = pd.melt(
            self.sales,
            id_vars=self.id_cols,
            var_name="date",
            value_name="sales",
        )
        self.sales["date"] = self.sales["date"].astype("category")

        # generate time features
        self.generate_time_features()
        self.time_features = self.time_features.astype(
            dict.fromkeys(
                self.time_features.drop(["dayofyear"], axis=1)
                .select_dtypes([np.int64, np.int32, np.int16])
                .columns,
                np.int8,
            )
        )
        self.sales = pd.merge(self.sales, self.time_features, on=["date"], how="left")
        del self.time_features

        # convert to day number and cast PLU to str
        self.sales = self.sales.astype(
            dict.fromkeys(
                self.sales.drop(["product_plu", "sales", "dayofyear"], axis=1)
                .select_dtypes([np.int64, np.int32, np.int16])
                .columns,
                np.int8,
            )
        )
        self.sales = self.convert_to_day_number(df=self.sales)
        self.sales["product_plu"] = self.sales["product_plu"].astype(str)

        # generate promo features
        self.generate_promo_features()
        self.promo_features = self.convert_to_day_number(df=self.promo_features)
        self.sales["product_plu"] = self.sales["product_plu"].astype("category")
        self.promo_features[["dc_code", "product_plu"]] = self.promo_features[
            ["dc_code", "product_plu"]
        ].astype("category")
        self.sales = pd.merge(
            self.sales,
            self.promo_features,
            on=["d", "dc_code", "product_plu"],
            how="left",
        )
        del self.promo_features

        # get promo columns and cast to category
        promo_cols = [x for x in self.sales.columns if x.startswith("promo_")]
        self.sales[promo_cols] = self.sales[promo_cols].fillna(0)
        self.sales[promo_cols] = self.sales[promo_cols].astype("category")

        for col in self.id_cols:
            self.sales[col] = self.sales[col].astype("category")

        self.sales.sort_values(by=["id", "d"], ignore_index=True, inplace=True)

        return reduce(self.sales)
