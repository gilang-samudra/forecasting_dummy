import datetime
import logging
import math
import os
import shutil
import uuid
from typing import Any, Dict, List, Tuple

import boto3
import pandas as pd
import psycopg2
from botocore.exceptions import ClientError

from src.config import ScheduleParam, DownloadLocation


ScheduleParam = ScheduleParam()

def thursday_outlet_group(master_delivery_group, today_date, download_path):
    old_monday_date = today_date - datetime.timedelta(days=today_date.weekday())
    old_delivery_group_path = os.path.join(
        download_path,
        f"delivery_group_rev_{old_monday_date.strftime('%Y%m%d')}.csv",
    )
    download(
        file_name=old_delivery_group_path,
        object_name=f"delivery_group_rev_{old_monday_date.strftime('%Y%m%d')}.csv",
    )
    old_outlet_group = pd.read_csv(old_delivery_group_path)

    # create the key for merging
    master_delivery_group["outlet_code_count"] = (
        master_delivery_group["outlet_code"]
        + "_"
        + master_delivery_group.groupby("outlet_code").cumcount().add(1).astype(str)
    )
    old_outlet_group["outlet_code_count"] = (
        old_outlet_group["outlet_code"]
        + "_"
        + old_outlet_group.groupby("outlet_code").cumcount().add(1).astype(str)
    )

    # merge dataframes to get old_outlet_group column
    old_outlet_group = old_outlet_group[["outlet_code_count", "outlet_group"]]
    old_outlet_group = old_outlet_group.rename(
        {"outlet_group": "old_outlet_group"}, axis=1
    )

    master_delivery_group = master_delivery_group.drop("old_outlet_group", axis=1)
    master_delivery_group = master_delivery_group.merge(
        old_outlet_group, on="outlet_code_count", how="left"
    )
    master_delivery_group = (
        master_delivery_group[
            [
                "dc_code",
                "outlet_code",
                "outlet_rank",
                "old_outlet_group",
                "outlet_group",
                "loading_number",
                "opening_date",
            ]
        ]
        .sort_values(by=["dc_code", "outlet_code"])
        .reset_index(drop=True)
    )

    del old_outlet_group
    return master_delivery_group


def download_essential_files(download_location) -> None:
    """Download file needed by normal calculation and recalculation."""

    download(file_name=download_location.SHELF_LIFE, object_name="shelf_life.csv")
    download(
        file_name=download_location.DAILY_TARGET,
        object_name="param/param_target_sample.csv",
    )  # ! temporary target


def download_recalculation_data(download_path, today_date, monday_date):
    download_location = DownloadLocation(download_path=download_path)
    download_essential_files(download_location)
    forecast_path = os.path.join(
        download_path, f"forecast_{today_date.strftime('%Y%m%d')}.csv"
    )
    type_path = os.path.join(
        download_path, f"store_type_{today_date.strftime('%Y%m%d')}.csv"
    )

    delivery_group_path = os.path.join(
        download_path, f"delivery_group_rev_{monday_date.strftime('%Y%m%d')}.csv"
    )

    sales_order_path = os.path.join(download_path, "sales_order_training_data.zip")

    download(
        file_name=forecast_path,
        object_name="archived/" + f"forecast_{today_date.strftime('%Y%m%d')}.csv",
        bucket_name="prod-forecast-712410194590-ap-southeast-1",
    )
    download(
        file_name=type_path,
        object_name="archived/" + f"store_type_{today_date.strftime('%Y%m%d')}.csv",
        bucket_name="prod-forecast-712410194590-ap-southeast-1",
    )

    download(
        file_name=delivery_group_path,
        object_name=f"delivery_group_rev_{monday_date.strftime('%Y%m%d')}.csv",
    )

    download(file_name=sales_order_path, object_name="sales_order_training_data.zip")

    # Read files
    forecast = pd.read_csv(forecast_path, sep="|")
    store_type = pd.read_csv(type_path, sep="|")
    master_delivery_group = pd.read_csv(delivery_group_path)
    shelf_life = pd.read_csv(download_location.SHELF_LIFE)
    daily_target = pd.read_csv(download_location.DAILY_TARGET)
    sales_order = pd.read_csv(sales_order_path)
    category_map = dict(zip(sales_order.product_plu, sales_order.category_id))

    # generate old_outlet_group if Thursday
    if today_date.strftime("%a") == "Thu":
        master_delivery_group = thursday_outlet_group(
            master_delivery_group, today_date, download_path
        )

    return (
        forecast,
        store_type,
        master_delivery_group,
        shelf_life,
        daily_target,
        category_map,
    )


def download_dataframes(download_path, today_date, monday_date):
    # Download files
    sales_path = os.path.join(download_path, "sales_training_data.csv")
    promo_path = os.path.join(download_path, "promotion_training_data.csv")
    delivery_group_path = os.path.join(
        download_path, f"delivery_group_rev_20230313.csv"
    )

    # Read files
    sales_init = pd.read_csv(sales_path)
    sales_init.drop(["customer_id"], axis=1, inplace=True)
    promo_data = pd.read_csv(promo_path)
    master_delivery_group = pd.read_csv(delivery_group_path)

    return (
        sales_init,
        promo_data,
        master_delivery_group,
    )



def generate_date(
    current_date: datetime.datetime, train_start: str = "2021-06-01"
) -> List[datetime.datetime]:
    """Generate training and submission window from current date."""

    train_start = datetime.datetime.strptime(train_start, "%Y-%m-%d")
    train_end = current_date - datetime.timedelta(days=3)
    forecast_start = current_date - datetime.timedelta(days=2)
    forecast_end = current_date + datetime.timedelta(days=9)

    return [train_start, train_end, forecast_start, forecast_end]


def make_directory(dirname: str) -> None:
    """Create directory to store training files."""
    if os.path.exists(dirname):
        shutil.rmtree(dirname)
    os.makedirs(dirname)


def rounding(x: float, t: float) -> float:
    """Round number according to specified threshold."""
    return math.floor(x) if (x - math.floor(x)) < t else math.ceil(x)


def filter_id(df: pd.DataFrame, col: str, selected: List[Any]) -> pd.DataFrame:
    """Filter ID based on selected IDs."""
    filtered = df.loc[df[col].isin(selected)].reset_index(drop=True)

    return filtered


def generate_schedule_information(
    df: pd.DataFrame, today_day: str
) -> Tuple[List[str], List[str], pd.DataFrame, pd.DataFrame]:
    """Generates scheduled outlets, scheduled DCS, outlet group, changed schedule, and outlet rank."""

    # get outlet code and outlet rank
    rank = df[["outlet_code", "outlet_rank"]]

    # reformat group schedule for DataFrame
    delivery_dict = {}
    group = []
    day = []

    for k, vals in ScheduleParam.DELIVERY_SCHEDULE.items():
        for v in vals:
            group.append(k)
            day.append(v)
    delivery_dict["outlet_group"] = group
    delivery_dict["delivery_day"] = day

    # generate forecast day column
    schedule_df = pd.DataFrame(data=delivery_dict)

    df_old = df.drop(["outlet_group"], axis=1)
    df_old.dropna(inplace=True)
    df_old.rename({"old_outlet_group": "outlet_group"}, axis=1, inplace=True)
    df_old = pd.merge(df_old, schedule_df, on=["outlet_group"], how="left")
    df_old["forecast_day"] = df_old["delivery_day"].map(
        ScheduleParam.DELIVERY_TO_FORECAST
    )
    df_old.rename(
        {
            "outlet_group": "old_outlet_group",
            "forecast_day": "old_forecast_day",
            "delivery_day": "old_delivery_day",
        },
        axis=1,
        inplace=True,
    )

    df.drop(["old_outlet_group"], axis=1, inplace=True)
    df = pd.merge(df, schedule_df, on=["outlet_group"], how="left")
    df["forecast_day"] = df["delivery_day"].map(ScheduleParam.DELIVERY_TO_FORECAST)

    df = pd.merge(df, df_old, on=["dc_code", "outlet_code"], how="left")

    # create DataFrame with scheduled outlets and corresponding outlet group(s)
    if today_day == "Thu":
        # get unchanged and changed group(s)
        unchanged = df.loc[
            (df["outlet_group"] == df["old_outlet_group"])
            & (df["forecast_day"] == today_day)
        ].reset_index(drop=True)
        unchanged = unchanged[["outlet_code", "outlet_group"]]
        changed = df.loc[(df["outlet_group"] != df["old_outlet_group"])].reset_index(
            drop=True
        )

        # get old outlet group(s) that still need to be forecasted for delivery on Sun
        old_group = changed.loc[(changed["old_delivery_day"] == "Sun")].reset_index(
            drop=True
        )
        old_group = old_group[["outlet_code", "old_outlet_group"]]
        old_group.rename({"old_outlet_group": "outlet_group"}, axis=1, inplace=True)
        old_group.drop_duplicates(inplace=True, ignore_index=True)

        # get new outlet group(s) that need to be forecasted for delivery on Mon
        new_group = changed.loc[(changed["delivery_day"] == "Mon")].reset_index(
            drop=True
        )
        new_group = new_group[["outlet_code", "outlet_group"]]

        # concatenate unchanged, old, and new groups
        group = pd.concat([unchanged, old_group, new_group], ignore_index=True)
        group.drop_duplicates(inplace=True, ignore_index=True)
        group.sort_values(by="outlet_code", ignore_index=True, inplace=True)

    else:
        group = df.loc[df["forecast_day"] == today_day][
            ["outlet_code", "outlet_group"]
        ].reset_index(drop=True)
        changed = pd.DataFrame()

    # create list of outlets and DCs
    outlets = group["outlet_code"].unique().tolist()
    dcs = df.loc[df["outlet_code"].isin(outlets)]["dc_code"].unique().tolist()

    # drop duplicates
    group = group.drop_duplicates(ignore_index=True)
    rank = rank.drop_duplicates(ignore_index=True)

    return outlets, dcs

