import pandas as pd
import numpy as np
from scipy.stats import mannwhitneyu
from typing import List


def infer_store_type(df: pd.DataFrame, outlet_list: List[str]) -> pd.DataFrame:
    """Store sales type inference by outlet-category level"""

    # Infer from last 56 days data
    df = df[["outlet_code", "category_id"] + df.columns[-56:].tolist()]
    df = df.sort_values(by=["outlet_code", "category_id"]).reset_index(drop=True)
    df = pd.melt(
        df[df["outlet_code"].isin(outlet_list)],
        id_vars=["outlet_code", "category_id"],
        var_name=["date"],
        value_name="sales",
    )
    df["date"] = pd.to_datetime(df["date"])
    df["day"] = df["date"].dt.dayofweek
    df["week_kind"] = df["day"].apply(
        lambda x: "weekday" if (x >= 0) & (x < 5) else "weekend"
    )

    # Separate to weekday or weekend data
    df_day = df[(df["week_kind"] == "weekday")]
    df_day = (
        df_day.groupby(["outlet_code", "category_id", "date"])["sales"]
        .sum()
        .reset_index()
    )
    df_end = df[(df["week_kind"] == "weekend")]
    df_end = (
        df_end.groupby(["outlet_code", "category_id", "date"])["sales"]
        .sum()
        .reset_index()
    )

    # Infer store sales type using percentage change and Mann-Whitney test
    all_id, id_index_day = np.unique(
        df_day.loc[:, ["outlet_code", "category_id"]].values.astype("<U64"),
        axis=0,
        return_index=True,
    )
    id_index_end = np.unique(
        df_end.loc[:, ["outlet_code", "category_id"]].values.astype("<U64"),
        axis=0,
        return_index=True,
    )[1]
    df_day_sales = np.split(df_day.sales.values, id_index_day[1:])
    df_end_sales = np.split(df_end.sales.values, id_index_end[1:])

    store_type_result = [
        mann_whitney_inference(df_day_sales[i], df_end_sales[i])
        for i in range(len(df_day_sales))
    ]
    store_type_result = np.column_stack([all_id, store_type_result])
    store_type_result = pd.DataFrame(
        store_type_result, columns=["outlet_code", "category_id", "store_type"]
    )

    return store_type_result


def mann_whitney_inference(weekday: np.array, weekend: np.array) -> str:
    """Infer store sales type using Mann-Whitney U test"""
    if weekday.mean() >= weekend.mean():
        mann_w_test = mannwhitneyu(weekday, weekend, alternative="greater")[1]
        store_type = "normal" if mann_w_test > 0.05 else "weekday"
    else:
        mann_w_test = mannwhitneyu(weekend, weekday, alternative="greater")[1]
        store_type = "normal" if mann_w_test > 0.05 else "weekend"

    return store_type
