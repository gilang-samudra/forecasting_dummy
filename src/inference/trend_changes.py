import pandas as pd
import numpy as np
import datetime
from calendar import monthrange
from typing import List


def trend_changes(
    reference_data: pd.DataFrame, sales_data_actual: pd.DataFrame
) -> pd.DataFrame:
    """Measure trend percentage changes"""

    # Filter id based on reference data
    sales_data_actual = sales_data_actual.loc[
        sales_data_actual.id.isin(reference_data.id)
    ]

    # Sort all dataframe by id
    sales_data_actual = sales_data_actual.sort_values(by="id").reset_index(drop=True)
    reference_data = reference_data.sort_values(by="id").reset_index(drop=True)

    all_result = reference_data[["id", "start_at"]].copy()

    # Cast id column to object
    all_result["id"] = all_result["id"].astype("object")

    # Iterate through three trend types
    for trend_type in ["last_week", "last_month", "last_year"]:
        all_data = combine_df(
            sales_data=sales_data_actual,
            reference_data=reference_data,
            trend_type=trend_type,
        )

        # Append the trend result
        all_result = all_result.merge(all_data, how="left", on=["id", "start_at"])
        all_result[f"unit_change_{trend_type}"] = (
            all_result["expected_sales"] - all_result["actual_expected_sales"]
        )
        all_result[f"trend_{trend_type}"] = (
            all_result[f"unit_change_{trend_type}"]
            / all_result["actual_expected_sales"]
        )
        all_result = all_result.rename(
            columns={"actual_expected_sales": f"sales_{trend_type}"}
        )
        all_result = all_result.drop(["expected_sales"], axis=1)

        # Replace inf trend values (case 0 historical sales) with 0
        all_result[f"trend_{trend_type}"] = np.where(
            all_result[f"trend_{trend_type}"].isin([np.inf, -np.inf]),
            0,
            all_result[f"trend_{trend_type}"],
        )

    # Fill nan values (case 0/0 and unavailable last year data) with 0
    all_result.fillna(0, inplace=True)

    # Renaming column
    all_result.rename({"start_at": "delivery_date"}, axis=1, inplace=True)

    all_result = all_result[
        [
            col
            for col in all_result.columns
            if col not in ["sales_last_week", "sales_last_month", "sales_last_year"]
        ]
        + ["sales_last_week", "sales_last_month", "sales_last_year"]
    ]

    return all_result


def combine_df(
    sales_data: pd.DataFrame, reference_data: pd.DataFrame, trend_type: str
) -> pd.DataFrame:
    """Merge ground truth and forecast dataset based on trend_type"""

    # Generate date list
    reference_data = reference_data.sort_values(by=["start_at", "end_at"])
    dates_pair, counts = np.unique(
        reference_data.loc[:, ["start_at", "end_at"]].values.astype("<U64"),
        axis=0,
        return_counts=True,
    )
    date_list_reference_1 = [
        date_list(pair[0], pair[1], sales_data.columns, trend_type)
        for pair in dates_pair
    ]
    date_list_reference_1 = [
        [all_date] * count for (all_date, count) in zip(date_list_reference_1, counts)
    ]
    reference_data = reference_data.assign(
        date_list_reference=[
            date for date_list_all in date_list_reference_1 for date in date_list_all
        ]
    )
    reference_data.sort_index(inplace=True)

    # Calculate expected sales of each id
    df = sales_data.copy().merge(reference_data, on="id")

    result = reference_data[["id", "start_at", "expected_sales"]].copy()
    result["actual_expected_sales"] = np.apply_along_axis(
        func1d=add_expected_sales, axis=1, arr=df.values, cols=df.columns.tolist()
    )

    return result


def add_expected_sales(arr: np.array, cols: List[str]) -> float:
    """Calculate expected sales with halving parameter"""

    half_start, half_end = arr[cols.index("half_start")], arr[cols.index("half_end")]
    sales = arr[np.where(np.isin(cols, arr[cols.index("date_list_reference")]))[0]]

    if half_start:
        sales[0] = 0.5 * sales[0]
    if half_end:
        sales[-1] = 0.5 * sales[-1]

    expect_sales = np.sum(sales)

    return expect_sales


def date_list(
    start_date: datetime.date,
    end_date: datetime.date,
    sales_data_columns: List[str],
    trend_type: str,
) -> List[str]:
    """Generate list of corresponding dates to certain trend type"""

    start_date = pd.to_datetime(start_date)
    end_date = pd.to_datetime(end_date)

    if trend_type == "last_week":
        date_list = last_week_date(
            start_date=start_date,
            end_date=end_date,
            sales_data_columns=sales_data_columns,
        )
    elif trend_type == "last_month":
        date_list = last_month_date(start_date=start_date, end_date=end_date)
    elif trend_type == "last_year":
        date_list = last_year_date(start_date=start_date, end_date=end_date)
    else:
        raise ValueError("Wrong trend change type")

    return date_list


def last_week_date(
    start_date: datetime.date, end_date: datetime.date, sales_data_columns: List[str]
) -> List[str]:
    """Generate last week sales data"""

    date_flag = (pd.to_datetime(end_date) - pd.Timedelta(days=7)).strftime(
        "%Y-%m-%d"
    ) in sales_data_columns
    if date_flag:
        date_list = (
            pd.date_range(
                start=start_date - pd.Timedelta(weeks=1),
                end=end_date - pd.Timedelta(weeks=1),
            )
            .strftime("%Y-%m-%d")
            .tolist()
        )
    else:
        date_list = (
            pd.date_range(
                start=start_date - pd.Timedelta(weeks=2),
                end=end_date - pd.Timedelta(weeks=2),
            )
            .strftime("%Y-%m-%d")
            .tolist()
        )

    return date_list


def last_month_date(start_date: datetime.date, end_date: datetime.date) -> List[str]:
    """Generate last month sales data"""

    date_list = (
        pd.date_range(
            start=start_date - pd.Timedelta(weeks=4),
            end=end_date - pd.Timedelta(weeks=4),
        )
        .strftime("%Y-%m-%d")
        .tolist()
    )

    return date_list


def last_year_date(start_date: datetime.date, end_date: datetime.date) -> List[str]:
    """Generate last year sales data"""

    date_list = (
        pd.date_range(
            start=start_date - pd.Timedelta(weeks=52),
            end=end_date - pd.Timedelta(weeks=52),
        )
        .strftime("%Y-%m-%d")
        .tolist()
    )

    return date_list
