import datetime
import logging
from typing import Dict, List

import pandas as pd
import src.utils


class DayOfWeekAverage:
    def __init__(
        self,
        df: pd.DataFrame,
        windows: List[datetime.datetime],
        date_map: Dict[str, int],
    ) -> None:
        self.df = df
        (
            self.train_start,
            self.train_end,
            self.forecast_start,
            self.forecast_end,
        ) = windows
        self.date_map = date_map

        self.train_end_day = self.date_map[self.train_end.strftime("%Y-%m-%d")]
        self.forecast_start_day = self.date_map[
            self.forecast_start.strftime("%Y-%m-%d")
        ]
        self.forecast_end_day = self.date_map[self.forecast_end.strftime("%Y-%m-%d")]

        self.training_df = self.df.loc[
            (self.df["d"] >= self.train_end_day - 27)
            & (self.df["d"] <= self.train_end_day)
        ].reset_index(drop=True)
        self.submission_df = self.df.loc[
            (self.df["d"] >= self.forecast_start_day)
            & (self.df["d"] <= self.forecast_end_day)
        ].reset_index(drop=True)

        del self.df

    def compute(self) -> pd.DataFrame:
        """Calculate forecast using average of 28 days by same day of week."""
        # calculate average by ID and day of week
        def split_list(
            alist: List,
            wanted_parts: int,
        ):
            length = len(alist)
            return [
                alist[i * length // wanted_parts : (i + 1) * length // wanted_parts]
                for i in range(wanted_parts)
            ]

        SPLIT_INFERENCE = 10
        ids_list = split_list(
            alist=self.training_df["id"].unique().tolist(), wanted_parts=SPLIT_INFERENCE
        )

        self.result = pd.DataFrame()

        for i, ids in enumerate(ids_list):
            logging.info(
                f"======= Processing 28D group-{i} with {len(ids)} IDs ======="
            )
            by_dayofweek = self.training_df.loc[self.training_df["id"].isin(ids)][
                ["id", "dayofweek", "sales"]
            ].copy()
            by_dayofweek = by_dayofweek.groupby(["id", "dayofweek"], as_index=False)[
                "sales"
            ].mean()

            # merge with submission DataFrame
            submission_df = self.submission_df.loc[self.submission_df["id"].isin(ids)][
                ["id", "d", "dayofweek"]
            ]
            submission_df = pd.merge(
                submission_df, by_dayofweek, on=["id", "dayofweek"], how="left"
            )
            del by_dayofweek

            # get rename columns to convert d -> F1, F2, etc.
            rename_cols = {}
            unique_days = submission_df["d"].unique().tolist()
            unique_days.sort()
            for i, day in enumerate(unique_days):
                rename_cols[day] = f"F{i+1}"

            # pivot DataFrame
            submission_df = submission_df.pivot(index="id", columns="d", values="sales")
            submission_df.reset_index(inplace=True)
            submission_df.sort_values(by=["id"], ignore_index=True, inplace=True)

            # convert column names from d to corresponding F1, F2, etc.
            submission_df.rename(rename_cols, axis=1, inplace=True)

            self.result = pd.concat([self.result, submission_df], ignore_index=True)
            del submission_df

        del self.training_df, self.submission_df

        return self.result
