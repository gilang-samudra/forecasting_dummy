import datetime
import gc
import logging
import multiprocessing
import os
import sys
from typing import Dict, List, Tuple

sys.path.append(os.getcwd())

import numpy as np
import pandas as pd
import pytz
from downcast import reduce

from src.main_helper_light_version import (
    filter_delivery,
    bottom_up_process
    )

from src import utils_light_version as utils
from src.config import GeneralConfig
from src.preprocessing.preprocess import Preprocess

CLOSED_STATUS = [
    "TUTUP PERMANENT",
    "TUTUP SEMENTARA",
    "MESSENGER",
    "OUT OF RANGE",
    "TIDAK TERCOVER SALES",
]


def files_path():
    BASE_PATH = "./files"
    DOWNLOAD_PATH = os.path.join(BASE_PATH, "download")
    SUBMISSION_PATH = os.path.join(BASE_PATH, "submission")

    return BASE_PATH, DOWNLOAD_PATH, SUBMISSION_PATH


def today_func():
    tzone = pytz.timezone("Asia/Jakarta")
    today_date = datetime.datetime.now(tz=tzone)
    today_date = today_date.replace(
        tzinfo=None, hour=0, minute=0, second=0, microsecond=0
    )
    return today_date


def save_and_upload(today_date, submission_path, submission_bu):
    submission_bu_filename = "forecast_" + today_date.strftime("%Y%m%d") + ".csv"
    submission_bu_csv_path = os.path.join(submission_path, submission_bu_filename)
    submission_bu.sort_values(by="id", ignore_index=True).to_csv(
        submission_bu_csv_path, index=False, sep="|"
    )

def main():
    # CONFIGURATION SETTING
    # Set multiprocessing process to spawn
    multiprocessing.set_start_method("spawn", force=True)

    # Instantiate configs
    GeneralConfig()

    # Record processing start time
    start_time = datetime.datetime.now()

    # Get today's date and day
    date_string = "2022-10-12"
    today_date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    today_day = today_date.strftime("%a")
    logging.info(f"Forecast on {today_date.strftime('%d %B %Y')}")

    # Get monday date for delivery group reference
    if today_date.weekday() >= 3:
        monday_date = today_date + datetime.timedelta(
            days=abs(today_date.weekday() - 7)
        )
    else:
        monday_date = today_date - datetime.timedelta(days=today_date.weekday())

    # Create directories
    BASE_PATH, DOWNLOAD_PATH, SUBMISSION_PATH = files_path()

    # CONFIGURATION SETTING ENDS HERE

    ### * FORECAST FOR ALL DCs * ###
    # Download files
    (
        sales_init,
        promo_data,
        master_delivery_group,
    ) = utils.download_dataframes(DOWNLOAD_PATH, today_date, monday_date)

    master_delivery_group, delivery_group = filter_delivery(
        'all', master_delivery_group, CLOSED_STATUS, today_date, sales_init
    )

    (
        outlets,
        dcs
    ) = utils.generate_schedule_information(df=delivery_group, today_day=today_day)

    logging.info(f"Number of DCs: {len(dcs)}")
    logging.info(f"Number of outlets: {len(outlets)}")

    sales_filtered = utils.filter_id(
        df=sales_init, col="outlet_code", selected=outlets
    )
    sales_filtered = reduce(sales_filtered)

    # Get start date from promo data
    start_date = promo_data["date"][0]
    logging.info(start_date)

    # Get end date from sales data
    end_date = sales_filtered.columns.tolist()[-1]
    end_datetime = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    # Generate training and submission window
    windows = utils.generate_date(current_date=today_date, train_start=start_date)

    _, train_end, forecast_start, forecast_end = windows

    # if sales integration fails
    if train_end > end_datetime:
        logging.warning("Sales data not updated, using last week's forecast!")
        logging.info(f"End of sales: {end_datetime}, supposedly {train_end}.")
        logging.info(f"Finished in {datetime.datetime.now() - start_time}")
        sys.exit(0)


    del sales_init
    last_time = datetime.datetime.now()

    # * Preprocessing
    # preprocess data
    preprocess = Preprocess(
        sales=sales_filtered.copy(deep=True), promo=promo_data, windows=windows
    )

    preprocess.compute()
    preprocess.create_date_map()

    logging.info(
        f"Preprocessing took {(datetime.datetime.now() - last_time).seconds} s"
    )
    last_time = datetime.datetime.now()

    # * Calculate BU forecast result
    # only select data within the submission range
    submission_range = [
        x.strftime("%Y-%m-%d")
        for x in pd.date_range(start=forecast_start, end=forecast_end)
    ]


    submission_data_bu = bottom_up_process(
        preprocess,
        today_day,
        today_date,
        submission_range,
    )

    submission_bu = pd.concat(submission_data_bu)
    del submission_data_bu
    gc.collect()

    logging.info(
        f"Bottom-up processes took {(datetime.datetime.now() - last_time).seconds} s"
    )
        

    # delete unnecessary files
    del preprocess, sales_filtered
    gc.collect()

    save_and_upload(today_date, SUBMISSION_PATH, submission_bu)


    logging.info(f"Finished in {datetime.datetime.now() - start_time}")


if __name__ == "__main__":
    main()
