from typing import Dict

import numpy as np
import pandas as pd
from lightgbm import LGBMRegressor

import multiprocessing


def train_and_predict(
    df: pd.DataFrame,
    conv_id: Dict[int, str],
    n_days: int,
) -> pd.DataFrame:
    """Perform bottom-up training and generate predictions."""

    train_end = df["d"].max() - n_days

    test = df[df["d"] > train_end]
    test = test[["id", "d", "sales"]].copy()

    df.drop(
        [
            "id",
            "dc_code",
            "cluster_id",
            "outlet_code",
            "category_id",
            "sub_category_id",
            "product_plu",
        ],
        axis=1,
    )

    all_train = df.loc[df["d"] <= train_end]
    all_test = df.loc[df["d"] > train_end].drop("sales", axis=1)

    pool = multiprocessing.Pool(4)
    data_inputs = []
    for g, grp in all_train.groupby("outlet_code"):
        data_inputs.append((all_test, grp, g))

    results = pool.starmap(train, data_inputs)
    test_preds_pool = pd.concat(results).sort_index()
    pool.close()
    pool.join()

    del df

    # Submission
    test["sales"] = test_preds_pool
    forecast = test[["id", "d", "sales"]].copy()
    forecast = pd.pivot(forecast, index="id", columns="d", values="sales").reset_index()

    del test, test_preds_pool

    forecast.columns = ["id"] + ["F" + str(i + 1) for i in range(n_days)]

    forecast.id = forecast.id.map(conv_id)

    return forecast


def train(
    all_test: pd.DataFrame,
    df_outlet: pd.DataFrame,
    outlet: str,
):
    X_train = np.array(df_outlet.drop("sales", axis=1))
    y_train = np.array(df_outlet["sales"])

    X_test = all_test.loc[all_test["outlet_code"] == outlet]

    model = LGBMRegressor(objective="rmse", seed=20, n_jobs=1)
    model.fit(X_train, y_train, eval_metric="rmse")

    return pd.Series(np.abs(model.predict(np.array(X_test))), index=X_test.index)
